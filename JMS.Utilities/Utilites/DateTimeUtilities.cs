﻿using System;

namespace JMS.Utilites
{
    public static class DateTimeUtilities
    {
        public static DateTime GetDateTimeFromEpochSecond(int secondsFromEpoch)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(secondsFromEpoch);
        }
    }
}
