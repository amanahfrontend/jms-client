﻿using System;
using System.IO;

namespace JMS.Utilites
{
    public static class FileUtilities
    {
        public static void CreateIfMissing(string path)
        {
            bool folderExists = Directory.Exists(path);
            if (!folderExists) Directory.CreateDirectory(path);
        }
        public static void WriteFileToPath(string fileBase64, string filePath)
        {
            using var stream = File.Create(filePath);
            File.SetAttributes(filePath, FileAttributes.Normal);
            var bytes = Convert.FromBase64String(fileBase64);
            stream.Write(bytes, 0, bytes.Length);
        }
        public static void LogToPath(string Logging, string filePath)
        {
            File.WriteAllText(filePath, Logging);
        }

    }
}
