﻿using System.Collections.Generic;

namespace JMS.Utilites
{
    public class SignalRConnection
    {
        public string ConnectionId { get; set; }
        public string UserId { get; set; }
        public List<string> Roles { get; set; }
        public string Token { get; set; }
    }
}
