﻿using DocumentFormat.OpenXml.Packaging;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace JMS.Utilities.Utilites
{
    public class DocHelper
    {
        public static void SearchAndReplace(string document, Dictionary<string, string> dict)
        {
            using WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true);
            string docText = null;
            using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
            {
                docText = sr.ReadToEnd();
            }

            foreach (KeyValuePair<string, string> item in dict)
            {
                Regex regexText = new Regex(item.Key);
                docText = regexText.Replace(docText, item.Value);
            }

            using StreamWriter sw = new StreamWriter(
                      wordDoc.MainDocumentPart.GetStream(FileMode.Create));
            sw.Write(docText);
        }
        public static void CopyFile(string sourcePath, string srcFileName, string targetPath , string targetFileName)
        {
            string sourceFile = Path.Combine(sourcePath, srcFileName);
            string destFile = Path.Combine(targetPath, targetFileName);
            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }

             File.Copy(sourceFile, destFile, true);
        }
        public static string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }
        private static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}

