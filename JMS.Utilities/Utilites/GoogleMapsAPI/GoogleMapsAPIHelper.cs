﻿using Newtonsoft.Json;
using System.Collections.Specialized;

namespace JMS.Utilites.GoogleMapsAPI
{
    public static class GoogleMapsAPIHelper
    {
        /// <summary>
        /// Get travel time using google maps directions api.
        /// </summary>
        /// <param name="key">Google maps api Key.</param>
        /// <param name="originLat">Origin Latitude.</param>
        /// <param name="originLong">Origin Longitude.</param>
        /// <param name="destinationLat">Destination Latitude.</param>
        /// <param name="destinationLong">Destination Longitude.</param>
        /// <returns>Time in seconds.</returns>
        public static long GetTravelTime(string serviceUrl, string key, double originLat, double originLong,
                                            double destinationLat, double destinationLong)
        {
            string originCoors = string.Format("{0},{1}", originLat, originLong);
            string destinationCoors = string.Format("{0},{1}", destinationLat, destinationLong);

            string queryString = UrlUtilities.ToQueryString(new NameValueCollection
                {
                    {"origin",originCoors },
                    {"destination",destinationCoors },
                    {"key",key },
                    {"departure_time","now" }
                });

            string url = serviceUrl + queryString;

            string response = UrlUtilities.GetServerResponse(url);

            var googleDirectionsModel = JsonConvert.DeserializeObject<GoogleDirectionsModel>(response);
            long seconds = googleDirectionsModel.Routes[0].Legs[0].DurationInTraffic.Value;
            return seconds;
        }
    }
}
