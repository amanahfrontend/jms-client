﻿using System;

namespace JMS.Utilites.UploadFile
{
    public class UploadFile : IUploadFile
    {
        public  string CreatedBy_Id { get; set; }
        public  string UpdatedBy_Id { get; set; }
        public  string DeletedBy_Id { get; set; }
        public string Tenant_Id { set; get; }
        public  bool IsDeleted { get; set; }
        public  DateTime CreatedDate { get; set; }
        public  DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }

        public string FileContent { get; set; }
        public string FileName { get; set; }
        public string FileRelativePath { get; set; }
    }
}
