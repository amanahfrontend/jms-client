﻿
using JMS.Utilites.ProcessingResult;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Utilites.UploadFile
{
    public class UploadImageFileManager : UploadFileManager
    {
        private List<string> imageExtensions = new List<string>()
    {
      ".png",
      ".bmp",
      ".jpg",
      ".jpeg",
      ".tif",
      ".gif"
    };

        public override ProcessResult<string> AddFile(Utilites.UploadFile.UploadFile file, string filePath)
        {
            ProcessResult<string> processResult1 = new ProcessResult<string>();
            try
            {
                ProcessResult<string> processResult2 = this.ISFileNull(file);
                if (processResult2.IsSucceeded)
                {
                    if (this.imageExtensions.Contains(Path.GetExtension(file.FileName).ToLower()))
                    {
                        processResult1 = base.AddFile(file, filePath);
                    }
                    else
                    {
                        processResult1.IsSucceeded = false;
                        processResult1.Message = "File must be of image extension";
                    }
                }
                else
                    processResult1 = processResult2;
            }
            catch (Exception ex)
            {
                processResult1.Exception = ex;
                processResult1.IsSucceeded = false;
            }
            return processResult1;
        }
    }
}
