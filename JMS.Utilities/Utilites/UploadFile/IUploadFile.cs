﻿using System;

namespace JMS.Utilites.UploadFile
{
    public interface IUploadFile
    {
        string CreatedBy_Id { get; set; }
        string UpdatedBy_Id { get; set; }
        string DeletedBy_Id { get; set; }
        string Tenant_Id { set; get; }
        bool IsDeleted { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime UpdatedDate { get; set; }
        DateTime DeletedDate { get; set; }

        string FileContent { get; set; }
        string FileName { get; set; }
        string FileRelativePath { get; set; }
    }
}