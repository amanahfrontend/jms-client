﻿using System;
using System.Collections.Generic;
using System.Net;

namespace JMS.Utilites.ProcessingResult
{
    public class ProcessResult<T>
    {
        public string Message;
        public string MethodName;
        public bool IsSucceeded { get; set; }
        public HttpStatusCode Status { get; set; }
        public Exception Exception { get; set; }
        public T ReturnData { get; set; }
        
        public ProcessResult(string methodName)
        {
            this.MethodName = methodName;
        }
        public ProcessResult()
        {
        }
    }
}
