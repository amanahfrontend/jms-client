﻿namespace Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel
{
    public class PaginatedItemsViewModel
    {
        //const int MaxPageSize = 10;
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchBy { get; set; }
        public int Id { get; set; }
        public int TotalNumbers { get; set; }
    }
}
