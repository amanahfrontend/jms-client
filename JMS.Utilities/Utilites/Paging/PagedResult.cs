﻿using System.Collections.Generic;

namespace JMS.Utilities.Utilites.Paging
{
    public class PagedResult<T>
    {
        public List<T> Result { set; get; }
        public int TotalCount { set; get; }

    }
}
