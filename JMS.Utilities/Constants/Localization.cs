﻿namespace JMS.Utilities.Constants
{
    public class Localization
    {
        public const string RESOURCES_PATH = "Resources";
        public const string COOKIES_LOCALIZATION = "UserCulture";
        public const string ARABIC = "ar";
        public const string ENGLISH = "en";
    }
}
