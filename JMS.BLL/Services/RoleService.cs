﻿using JMS.BLL.Interfaces;
using JMS.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace JMS.BLL.Services
{
    public class RoleService : IRoleService
    {
        public RoleService()
        {

        }

        public List<Role> GetAllRoles()
        {
            throw new System.NotImplementedException();
        }
        public Role GetRoleBy(Expression<Func<Role, bool>> predicate)
        {
            throw new NotImplementedException();
        }
        public Role GetRoleByName(string roleName)
        {
            throw new System.NotImplementedException();
        }
        public Role AddRole(Role entityResult)
        {
            throw new System.NotImplementedException();
        }
        public bool UpdateRole(Role entityResult)
        {
            throw new System.NotImplementedException();
        }
        public bool DeleteRole(Role entity)
        {
            throw new System.NotImplementedException();
        }

      
    }
}
