﻿using JMS.BLL.Common;
using JMS.BLL.Interfaces;
using JMS.DAL.Common.Enums;
using JMS.DAL.Context;
using JMS.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace JMS.BLL.Services
{
    public class ManagerService : IManagerService
    {
        private readonly DatabaseContext _context;

        public ManagerService(DatabaseContext context) =>_context = context;
        public Manager GetFirstOrDefaultBy(Expression<Func<Manager, bool>> predicate) => _context.Managers.Include(e => e.User).ThenInclude(e => e.Role).FirstOrDefault(predicate);
        public List<Manager> GetBy(Expression<Func<Manager, bool>> predicate) => _context.Managers.Include(e => e.User).ThenInclude(e => e.Role).Where(predicate).ToList();
        public ServiceResponse<List<User>> GetManagers(string managerName = "")
        {
            ServiceResponse<List<User>> response = new ServiceResponse<List<User>>();
            try
            {
                response.Data = _context.Managers
                                        .Include(e => e.User).ThenInclude(e => e.Role)
                                        .Where(e => e.User.Role.UserRoleType != UserRoleType.Admin && e.User.Role.UserRoleType != UserRoleType.Driver
                                                        && string.IsNullOrEmpty(managerName) || e.User.FullName.Contains(managerName))
                                        .Select(e => e.User).ToList();
                response.Status = ResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Status = ResponseStatus.ServerError;
                ExceptionLogger.LogException(ex);
            }
            return response;
        }

        public Manager Create(Manager manager)
        {
            _context.Managers.Add(manager);
            _context.SaveChanges();
            return manager;
        }
        public Manager Update(Manager manager)
        {
            _context.Managers.Update(manager);
            _context.SaveChanges();
            return manager;
        }
        public Manager Delete(Manager manager)
        {
            _context.Managers.Remove(manager);
            _context.SaveChanges();
            return manager;
        }

    }
}
