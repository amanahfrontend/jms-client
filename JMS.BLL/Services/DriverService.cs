﻿using JMS.BLL.Common;
using JMS.BLL.Interfaces;
using JMS.DAL.Common.Enums;
using JMS.DAL.Context;
using JMS.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace JMS.BLL.Services
{
    public class DriverService : IDriverService
    {
        private readonly DatabaseContext _context;
        private List<JourneyStatus> _endedJourneyStatuses = new List<JourneyStatus> { JourneyStatus.JourneyClosed, JourneyStatus.JourneyCompleted, JourneyStatus.JourneyRejected };

        public DriverService(DatabaseContext context)
        {
            _context = context;
        }
        public Driver GetFirstOrDefaultBy(Expression<Func<Driver, bool>> predicate) => _context.Drivers.Include(e => e.User).ThenInclude(e => e.Role).FirstOrDefault(predicate);
        public List<Driver> GetBy(Expression<Func<Driver, bool>> predicate) => _context.Drivers.Include(e => e.User).ThenInclude(e => e.Role).Where(predicate).ToList();
        public ServiceResponse<List<User>> GetDrivers(string driverName = "")
        {
            ServiceResponse<List<User>> response = new ServiceResponse<List<User>>();
            try
            {
                response.Data = _context.Drivers
                                                .Include(e=> e.User).ThenInclude(e=>e.Role)
                                                .Where(e => e.User.Role.UserRoleType == UserRoleType.Driver
                                                        && string.IsNullOrEmpty(driverName) || e.User.FullName.Contains(driverName)
                                                        && !e.IsInProgress && (e.LastCompletionTrip == null || EF.Functions.DateDiffHour(e.LastCompletionTrip.Value, DateTime.Now) > 14)).ToList()
                                                .Select(e => e.User).ToList();
                response.Status = ResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Status = ResponseStatus.ServerError;
                ExceptionLogger.LogException(ex);
            }
            return response;
        }

        public ServiceResponse SubmitAssessment(int journeyId, int? journeyUpdateId, JourneyStatus status, List<AssessmentResult> assessmentResult)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                var journey = _context.Journey.Find(journeyId);
                journey.JourneyStatus = status;

                if (journeyUpdateId.HasValue && journeyUpdateId.Value != 0)
                {
                    var journeyUpdate = _context.JourneyUpdate.Find(journeyUpdateId);
                    journeyUpdate.JourneyStatus = JourneyStatus.PendingOnJMCApproveDriverCheckpointAssessment;
                }
                if (assessmentResult.Count > 0)
                    _context.AssessmentResults.AddRange(assessmentResult);
                _context.SaveChanges();

                response.Status = ResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Status = ResponseStatus.ServerError;

                ExceptionLogger.LogException(ex);
            }

            return response;
        }
        public ServiceResponse SubmitStatus(JourneyUpdate journeyUpdate)
        {
            ServiceResponse response = new ServiceResponse();

            try
            {
                _context.JourneyUpdate.Add(journeyUpdate);
                _context.SaveChanges();

                response.Status = ResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Status = ResponseStatus.ServerError;

                ExceptionLogger.LogException(ex);
            }

            return response;
        }
        public ServiceResponse<List<AssessmentQuestion>> GetJourneyAssessment(int journeyId, bool isPostTrip = false, bool includeResults = false)
        {
            ServiceResponse<List<AssessmentQuestion>> response = new ServiceResponse<List<AssessmentQuestion>>();

            try
            {
                if (includeResults)
                {
                    if (isPostTrip)
                        response.Data = _context.AssessmentQuestions.Include(a => a.AssessmentResult).AsNoTracking()
                            .Where(q => (q.JourneyId == journeyId) && (q.CheckpointId == null) && (q.Category == QuestionCategory.PostTrip || q.Category == QuestionCategory.VehicleChecklist)).ToList();
                    else
                        response.Data = _context.AssessmentQuestions.Include(a => a.AssessmentResult).AsNoTracking()
                              .Where(q => (q.JourneyId == journeyId) && (q.CheckpointId == null) && (q.Category == QuestionCategory.PreTrip || q.Category == QuestionCategory.VehicleChecklist)).ToList();
                }
                else
                {
                    if (isPostTrip)
                        response.Data = _context.AssessmentQuestions
                            .Where(q => (q.JourneyId == journeyId) && (q.CheckpointId == null) && (q.Category == QuestionCategory.PostTrip || q.Category == QuestionCategory.VehicleChecklist)).ToList();
                    else
                        response.Data = _context.AssessmentQuestions
                            .Where(q => (q.JourneyId == journeyId) && (q.CheckpointId == null) && (q.Category == QuestionCategory.PreTrip || q.Category == QuestionCategory.VehicleChecklist)).ToList();
                }

                response.Status = ResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Status = ResponseStatus.ServerError;

                ExceptionLogger.LogException(ex);
            }

            return response;
        }
        public ServiceResponse<List<AssessmentQuestion>> GetCheckpointAssessment(int checkpointId, int journeyId, bool includeResults = false)
        {
            ServiceResponse<List<AssessmentQuestion>> response = new ServiceResponse<List<AssessmentQuestion>>();

            try
            {
                if (includeResults)
                    response.Data = _context.AssessmentQuestions.Include(a => a.AssessmentResult).AsNoTracking()
                        .Where(q => q.JourneyId == journeyId && ((q.Category == QuestionCategory.CheckpointAssessment && q.CheckpointId == checkpointId) || q.Category == QuestionCategory.VehicleChecklist)).ToList();
                else
                    response.Data = _context.AssessmentQuestions
                        .Where(q => q.JourneyId == journeyId && ((q.Category == QuestionCategory.CheckpointAssessment && q.CheckpointId == checkpointId) || q.Category == QuestionCategory.VehicleChecklist)).ToList();

                response.Status = ResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Status = ResponseStatus.ServerError;

                ExceptionLogger.LogException(ex);
            }

            return response;
        }

        public Driver Create(Driver driver)
        {
            driver.User = null;
            _context.Drivers.Add(driver);
            _context.SaveChanges();
            return driver;
        }
        public Driver Update(Driver driver)
        {
            _context.Drivers.Update(driver);
            _context.SaveChanges();
            return driver;
        }
        public Driver Delete(Driver driver)
        {
            //need to check if have open journey first
            _context.Drivers.Remove(driver);
            _context.SaveChanges();
            return driver;
        }


    }
}
