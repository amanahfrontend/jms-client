﻿using System;

namespace JMS.BLL.ViewModels
{
    public class RegisterModel
    {
        public int Id { get; set; }
        public Guid RowId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public int? UserGroupId { get; set; }
        public int? UserWorkForceId { get; set; }
        public string LicenseNo { get; set; }
        public DateTime? LicenseExpiryDate { get; set; }
        public string TrainingDetails { get; set; }
        public string GatePassStatus { get; set; }
        public int RoleId { get; set; }
        public bool IsActive { get; set; }
    }
}
