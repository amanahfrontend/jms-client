﻿using JMS.DAL.Entities;
using System;

namespace JMS.BLL.ViewModels
{
    public class UserRelatedToTeamOrWorkForceModel
    {
        public int Id { get; set; }
        public Guid RowId { get; set; }
        public string FullName { get; set; }
        public bool IsDriver { get; set; }
        public Role Role { get; set; }
    }
}
