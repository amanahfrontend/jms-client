﻿using JMS.DAL.Common.Enums;
using System;
using System.Collections.Generic;

namespace JMS.BLL.ViewModels
{
    public class RoleModel
    {
        public Guid RowId { get; set; }
        public string Name { get; set; }
        public UserRoleType UserRoleType { get; set; }
        public List<KeyValuePair<string, string>> Permissions { get; set; }
    }
}
