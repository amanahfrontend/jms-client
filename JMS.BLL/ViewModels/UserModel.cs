﻿using System;
using System.Collections.Generic;

namespace JMS.BLL.ViewModels
{
    public class UserModel
    {
        public int Id { get; set; }
        public Guid? RowId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public int? UserGroupId { get; set; }
        public int? WorkForceId { get; set; }
        public bool IsActive { get; set; }
        public string PhoneNumber { get; set; }
        public string EmergencyContact { get; set; }
        public string Nationality { get; set; }
        public string Address { get; set; }
        public string Comments { get; set; }
        public string Image { get; set; }
        public virtual List<string> Attachments { get; set; }
        public bool IsHavingActiveJourney { get; set; }
        public List<KeyValuePair<int, string>> ActiveJourneys { get; set; }
        public string Token { get; set; }
        public int? RoleId { get; set; }
        public RoleModel Role { get; set; }
    }
}
