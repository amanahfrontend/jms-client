﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JMS.BLL.ViewModels
{
    public class TeamModel
    {
        public int Id { get; set; }
        public Guid RowId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsHavingActiveJourney { get; set; }

        public List<UserRelatedToTeamOrWorkForceModel> Users { get; set; }
    }
}
