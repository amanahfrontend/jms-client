﻿using JMS.DAL.Enums;
using System;

namespace JMS.BLL.ViewModels
{
    public class DriverModel : UserModel
    {
        public int UserId { get; set; } 
        public string LicenseNo { get; set; }
        public DateTime? LicenseExpiryDate { get; set; }
        public VehicleType DriveFor { get; set; }
        public string TrainingDetails { get; set; }
        public string GatePassStatus { get; set; }
        public bool IsHavingChronicDiseases { get; set; }
        public string ChronicDiseases { get; set; }

    }
}
