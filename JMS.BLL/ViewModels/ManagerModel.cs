﻿namespace JMS.BLL.ViewModels
{
    public class ManagerModel : UserModel
    {
        public int UserId { get; set; }
        public string Position { get; set; }
    }
}
