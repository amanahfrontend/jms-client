﻿using JMS.BLL.Common;
using JMS.BLL.Models;
using JMS.DAL.Common.Enums;
using JMS.DAL.Entities;
using System;
using System.Collections.Generic;

namespace JMS.BLL.Interfaces
{
    public interface IUserService
    {
        User Authenticate(string username, string newpassword);

        public ServiceResponse<List<LookupModel<Guid>>> GetUsersByRole(UserRoleType role);
        Role GetRoleById(int id);
        public ServiceResponse<object> GetAllUsers();
        PageResult<User> GetAll(string keywordfilter, PagingProperties pagingProperties);
        User GetUserById(Guid id);
        User GetByName(string username);

        User Add(User user, string password);
        void Update(User user, string password = null);
        void Delete(Guid id);
        void ActivateDisactvate(Guid userId, bool isActive);
        ServiceResponse ChangePassword(Guid userId, string oldPassword, string newPassword);
        void ResetPassword(Guid userId, string randomPassword);
        ServiceResponse ForgetPassword(string username, string email, string emailPassword, string hosting);
        ServiceResponse ResetForgetPassword(string token, string newpassword);

      

        public ServiceResponse<List<UserGroup>> GetUserGroups();
        UserGroup GetUserGroupById(Guid rowId);
        public ServiceResponse<UserGroup> GetUserGroup(Guid rowId);
        public ServiceResponse<UserGroup> AddUserGroup(UserGroup group);
        public ServiceResponse EditUserGroup(UserGroup group);
        public ServiceResponse DeleteUserGroup(Guid groupId);

        public ServiceResponse<List<WorkForce>> GetWorkForces();
        public ServiceResponse<WorkForce> GetWorkForce(Guid rowId);
        public ServiceResponse<WorkForce> AddWorkForce(WorkForce workforce);
        public ServiceResponse EditWorkForce(WorkForce workforce);
        public ServiceResponse DeleteWorkForce(Guid workforceId);

    }
}
