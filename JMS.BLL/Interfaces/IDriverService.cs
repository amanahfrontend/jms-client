﻿using JMS.BLL.Common;
using JMS.DAL.Common.Enums;
using JMS.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace JMS.BLL.Interfaces
{
    public interface IDriverService
    {
        Driver GetFirstOrDefaultBy(Expression<Func<Driver, bool>> predicate);
        List<Driver> GetBy(Expression<Func<Driver, bool>> predicate);
        ServiceResponse<List<User>> GetDrivers(string driverName);
        ServiceResponse<List<AssessmentQuestion>> GetJourneyAssessment(int journeyId, bool isPreTrip, bool includeResults);
        ServiceResponse<List<AssessmentQuestion>> GetCheckpointAssessment(int checkpoint, int journeyId, bool includeResults);
        Driver Create(Driver driver);
        Driver Update(Driver driver);
        Driver Delete(Driver driver);
        ServiceResponse SubmitAssessment(int journeyId, int? journeyUpdateId, JourneyStatus status, List<AssessmentResult> assessmentResult);
        ServiceResponse SubmitStatus(JourneyUpdate journeyUpdate);

    }
}
