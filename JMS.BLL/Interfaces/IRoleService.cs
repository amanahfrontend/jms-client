﻿using JMS.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace JMS.BLL.Interfaces
{
    public interface IRoleService
    {
        List<Role> GetAllRoles();
        Role GetRoleBy(Expression<Func<Role, bool>> predicate);
        Role GetRoleByName(string roleName);
        Role AddRole(Role entityResult);
        bool UpdateRole(Role entityResult);
        bool DeleteRole(Role entity);
    }
}
