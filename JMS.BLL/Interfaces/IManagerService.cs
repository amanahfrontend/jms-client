﻿using JMS.BLL.Common;
using JMS.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace JMS.BLL.Interfaces
{
    public interface IManagerService
    {
        ServiceResponse<List<User>> GetManagers(string managerName = "");
        Manager GetFirstOrDefaultBy(Expression<Func<Manager, bool>> predicate);
        List<Manager> GetBy(Expression<Func<Manager, bool>> predicate);
        Manager Create(Manager manager);
        Manager Update(Manager manager);
        Manager Delete(Manager manager);
    }
}
