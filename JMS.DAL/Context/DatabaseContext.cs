﻿using JMS.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace JMS.DAL.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() { }
        public DatabaseContext(DbContextOptions<DbContext> options) : base(options) { }
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<WorkForce> WorkForces { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<AssessmentQuestion> AssessmentQuestions { get; set; }
        public virtual DbSet<AssessmentResult> AssessmentResults { get; set; }
        public virtual DbSet<Checkpoint> Checkpoints { get; set; }
        public virtual DbSet<CodeException> CodeException { get; set; }
        public virtual DbSet<Journey> Journey { get; set; }
        public virtual DbSet<JourneyUpdate> JourneyUpdate { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder) => CreateModel(modelBuilder);

        public void CreateModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasIndex(e => e.Name).IsUnique();

            //modelBuilder.ApplyConfiguration(new OrganizationalStructureConfiguration())
            //            .ApplyConfiguration(new OrganizationalStructureItemConfiguration())
            //            .ApplyConfiguration(new OrganizationalStructureConfiguration())
            //            .ApplyConfiguration(new OrganizationSettingConfiguration())
            //            .ApplyConfiguration(new OrganizationConfiguration());
        }
    }
}
