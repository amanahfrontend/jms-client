﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JMS.DAL.Migrations
{
    public partial class UpdateInitDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_UserWorkForces_UserWorkForceId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "UserWorkForces");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserRoleId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserWorkForceId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserWorkForceId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserRoleId",
                table: "UserRoles");

            migrationBuilder.AddColumn<int>(
                name: "WorkForceId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "UserGroups",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "WorkForces",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RowId = table.Column<Guid>(nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    DeletedById = table.Column<string>(nullable: true),
                    TenantId = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkForces", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserRoleId",
                table: "Users",
                column: "UserRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_WorkForceId",
                table: "Users",
                column: "WorkForceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_WorkForces_WorkForceId",
                table: "Users",
                column: "WorkForceId",
                principalTable: "WorkForces",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_WorkForces_WorkForceId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "WorkForces");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserRoleId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_WorkForceId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "WorkForceId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "UserGroups");

            migrationBuilder.AddColumn<int>(
                name: "UserWorkForceId",
                table: "Users",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleId",
                table: "UserRoles",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserWorkForces",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    RowId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenantId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserWorkForces", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserRoleId",
                table: "Users",
                column: "UserRoleId",
                unique: true,
                filter: "[UserRoleId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserWorkForceId",
                table: "Users",
                column: "UserWorkForceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_UserWorkForces_UserWorkForceId",
                table: "Users",
                column: "UserWorkForceId",
                principalTable: "UserWorkForces",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
