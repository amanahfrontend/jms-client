﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JMS.DAL.Migrations
{
    public partial class updateActivationDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrentDriverId",
                table: "Journey",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Journey_CurrentDriverId",
                table: "Journey",
                column: "CurrentDriverId");

            migrationBuilder.AddForeignKey(
                name: "FK_Journey_Drivers_CurrentDriverId",
                table: "Journey",
                column: "CurrentDriverId",
                principalTable: "Drivers",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Journey_Drivers_CurrentDriverId",
                table: "Journey");

            migrationBuilder.DropIndex(
                name: "IX_Journey_CurrentDriverId",
                table: "Journey");

            migrationBuilder.DropColumn(
                name: "CurrentDriverId",
                table: "Journey");
        }
    }
}
