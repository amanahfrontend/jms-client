﻿using JMS.Utilities.Utilites.Paging;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JMS.DAL.Core.Persistence.Interfaces.IRepoistries
{
    public interface IRepositry<TEntity>
    {
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetAll(bool ignoreTenant);
        Task<PagedResult<TEntity>> GetAllByPaginationAsync(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel);
        IQueryable<TEntity> GetAllByPagination(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel, out int totalNumbers);
        TEntity Get(params object[] id);
        TEntity Get(Expression<Func<TEntity, bool>> predicate);
        TEntity AddAsync(TEntity entity);
        List<TEntity> Add(List<TEntity> entityLst);
        bool Update(TEntity entity);
        bool Delete(TEntity entity);
        bool Delete(List<TEntity> entitylst);
        bool DeleteById(params object[] id);
        bool SoftDelete(TEntity entity);
        int SaveChanges();
    }
}