﻿namespace JMS.DAL.Core.Persistence.Interfaces.IManagers
{
    public interface ITenantService
    {
        string GetCurrentTenantId();
    }
}