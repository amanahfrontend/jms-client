﻿using JMS.Utilities.Utilites.Paging;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JMS.DAL.Core.Persistence.Interfaces.IManagers
{
    public interface IBaseManager<TViewModel, TEntity>
    {
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>();
        Task<PagedResult<TViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<PagedResult<TViewModel>> GetAllByPaginationAsync(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingParameterModel);
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(bool ignoreTenant);
        Task<PagedResult<TViewModel>> GetAllByPaginationAsync(bool ignoreTenant, PaginatedItemsViewModel pagingParameterModel);
        Task<TViewModel> AddAsync(TViewModel entity);
        Task<List<TViewModel>> AddAsync(List<TViewModel> entities);
        Task<bool> UpdateAsync(TViewModel entity);
        Task<bool> DeleteAsync(TViewModel entity);
        Task<bool> DeleteAsync(List<TViewModel> entities);
        Task<bool> DeleteByIdAsync(params object[] id);
        Task<bool> SoftDeleteAsync(TViewModel entity);
    }
}