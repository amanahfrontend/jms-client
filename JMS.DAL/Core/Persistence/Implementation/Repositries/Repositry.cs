﻿using JMS.DAL.Core.Common;
using JMS.DAL.Core.IEntities;
using JMS.DAL.Core.Persistence.Implementation.Managers;
using JMS.DAL.Core.Persistence.Interfaces.IRepoistries;
using JMS.Utilities.Utilites.Paging;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JMS.DAL.Core.Persistence.Implementation.Repoistries
{
    public class Repositry<T> : IRepositry<T> where T : class, IBaseEntity
    {
        private readonly DbSet<T> _set;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public DbContext Context { get; private set; }

        public Repositry(DbContext context, IHttpContextAccessor httpContextAccessor)
        {
            Context = context;
            _set = Context.Set<T>();
            _httpContextAccessor = httpContextAccessor;
        }

        public IQueryable<T> GetAll(bool ignoreTenant)
        {
            if (ignoreTenant) return _set.Where(x => x.IsDeleted == false).OrderByDescending(x => x.CreatedDate).AsNoTracking();
            else return _set.Where(x => x.IsDeleted == false && x.TenantId == GetCurrentTenantId()).OrderByDescending(x => x.CreatedDate).AsNoTracking();
        }
        public virtual IQueryable<T> GetAll() => _set.Where(x => x.IsDeleted == false && x.TenantId == GetCurrentTenantId()).OrderByDescending(x => x.CreatedDate).AsNoTracking();
        public virtual T Get(params object[] id) => _set.Find(id);
        public T Get(Expression<Func<T, bool>> predicate)
        {
            T item;
            item = _set.FirstOrDefault(predicate);
            return item;
        }
        public IQueryable<T> GetAllByPagination(IQueryable<T> listQuery, PaginatedItemsViewModel pagingparametermodel, out int totalNumbers)
        {
            if (pagingparametermodel.PageNumber == 0) pagingparametermodel.PageNumber = 1;
            var source = listQuery.AsQueryable().AsNoTracking();
            int currentPage = pagingparametermodel.PageNumber;// Parameter is passed from Query string if it is null then it default Value will be pageNumber:1
            int pageSize = pagingparametermodel.PageSize; // Parameter is passed from Query string if it is null then it default Value will be pageSize:20
            totalNumbers = source.Count();
            var items = source.Skip((currentPage - 1) * pageSize).Take(pageSize);

            return items;
        }
        public async Task<PagedResult<T>> GetAllByPaginationAsync(IQueryable<T> listQuery, PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new PagedResult<T>();
            if (pagingparametermodel.PageNumber == 0) pagingparametermodel.PageNumber = 1;
            var source = listQuery.AsQueryable().AsNoTracking();
            int currentPage = pagingparametermodel.PageNumber;  // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1
            int pageSize = pagingparametermodel.PageSize;         // Parameter is passed from Query string if it is null then it default Value will be pageSize:20 
            pagedResult.TotalCount = await source.CountAsync();//
            pagedResult.Result = source.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            return pagedResult;
        }
        public T Get(string property, object value)
        {
            var lambda = CreateEqualSingleExpression(property, value);
            return _set.SingleOrDefault(lambda);
        }


        public virtual T AddAsync(T entity)
        {
            T result = null;
            try
            {
                if (Validator.IsValid(entity))
                {
                    entity.TenantId = GetCurrentTenantId();
                    entity.CreatedById = this._httpContextAccessor.HttpContext.User.Identity.Name;
                    entity.IsDeleted = false;
                    entity = _set.Add(entity).Entity;
                    if (SaveChanges() > 0) result = entity;
                }
                else
                {
                    StringBuilder exceptionMsgs = new StringBuilder();
                    List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                    foreach (var errmsg in errorMsgs)
                    {
                        exceptionMsgs.Append(errmsg);
                        exceptionMsgs.Append("/n");
                    }
                    throw new Exception(exceptionMsgs.ToString());
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return result;
        }
        public virtual void Add(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.TenantId = GetCurrentTenantId();
                entity.CreatedById = this._httpContextAccessor.HttpContext.User.Identity.Name;
                _set.Add(entity);
            }
            SaveChanges();
        }
        public virtual List<T> Add(List<T> entities)
        {
            List<T> objList = new List<T>();
            foreach (T entity in entities) objList.Add(AddAsync(entity));
            return objList;
        }

        public virtual bool Update(T entity)
        {
            bool result = false;
            if (Validator.IsValid(entity))
            {
                entity.UpdatedById = this._httpContextAccessor.HttpContext.User.Identity.Name;
                Context.Entry<T>(entity).State = EntityState.Modified;
                Context.Entry(entity).Property(x => x.CreatedById).IsModified = false;
                Context.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                Context.Entry(entity).Property(x => x.TenantId).IsModified = false;

                if (SaveChanges() > 0)
                {
                    Context.Entry<T>(entity).State = EntityState.Detached;
                    result = true;
                }
                Context.Entry<T>(entity).State = EntityState.Detached;
            }
            else
            {
                StringBuilder exceptionMsgs = new StringBuilder();
                List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                foreach (var errmsg in errorMsgs)
                {
                    exceptionMsgs.Append(errmsg);
                    exceptionMsgs.Append("/n");
                }
                throw new Exception(exceptionMsgs.ToString());
            }
            return result;
        }
        public virtual bool Update(IEnumerable<T> entities)
        {
            bool result = false;
            foreach (var entity in entities)
            {
                entity.UpdatedById = this._httpContextAccessor.HttpContext.User.Identity.Name;
                if (Validator.IsValid(entity))
                {
                    Context.Entry<T>(entity).State = EntityState.Modified;
                    Context.Entry(entity).Property(x => x.CreatedById).IsModified = false;
                    Context.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    Context.Entry(entity).Property(x => x.TenantId).IsModified = false;
                }
                else
                {
                    StringBuilder exceptionMsgs = new StringBuilder();
                    List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                    foreach (var errmsg in errorMsgs)
                    {
                        exceptionMsgs.Append(errmsg);
                        exceptionMsgs.Append("/n");
                    }
                    throw new Exception(exceptionMsgs.ToString());
                }
                if (SaveChanges() > 0) result = true;
            }
            return result;
        }

        public virtual bool Delete(T entity)
        {
            entity.DeletedById = this._httpContextAccessor.HttpContext.User.Identity.Name;
            Context.Entry<T>(entity).State = EntityState.Deleted;
            return Context.SaveChanges() > 0;
        }
        public virtual bool DeleteById(params object[] id)
        {
            T entity = _set.Find(id);
            entity.DeletedById = this._httpContextAccessor.HttpContext.User.Identity.Name; 
            return Delete(entity);
        }
        public virtual bool Delete(List<T> entitylst)
        {
            bool result = false;
            if (entitylst != null && entitylst.Count > 0)
            {
                foreach (var entity in entitylst)
                {
                    entity.DeletedById = this._httpContextAccessor.HttpContext.User.Identity.Name;
                    result = Delete(entity);
                }
                SaveChanges();
            }
            return result;
        }
        public bool SoftDelete(T entity)
        {
            entity.IsDeleted = true;
            return Update(entity);
        }

        public virtual void RollBack() { }
        public virtual void Commit() {}
        public virtual int SaveChanges()
        {
            BaseEntityManager.AddAuditingData(Context.ChangeTracker.Entries());
            return Context.SaveChanges();
        }
        public Expression<Func<T, bool>> CreateEqualSingleExpression(string property, object value)
        {
            var parameterExpression = Expression.Parameter(typeof(T));
            var propertyExpression = Expression.Property(parameterExpression, property);
            var equalsExpression = Expression.Equal(propertyExpression, Expression.Constant(value));
            return Expression.Lambda<Func<T, bool>>(equalsExpression, parameterExpression);
        }

        private string GetCurrentTenantId()
        {
            if (_httpContextAccessor.HttpContext.User != null)
                if (_httpContextAccessor.HttpContext.User.Claims.Count() > 0) return _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == "TenantId").FirstOrDefault().Value;

            return null;
        }
        private string GetCurrentUser_Name() => _httpContextAccessor.HttpContext.User.Identity.Name;
    }
}
