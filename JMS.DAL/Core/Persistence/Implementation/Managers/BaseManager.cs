﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using JMS.DAL.Core.Persistence.Interfaces.IManagers;
using JMS.DAL.Core.Persistence.Interfaces.IRepoistries;
using JMS.Utilities.Utilites.Paging;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JMS.DAL.Core.Persistence.Implementation.Managers
{
    public class BaseManager<TViewModel, TEntity> : IBaseManager<TViewModel, TEntity>
    {
        protected readonly IRepositry<TEntity> _repository;
        protected readonly DbContext _context;
        protected readonly IMapper _mapper;
        readonly string[] _membersToExpand;

        public BaseManager(DbContext context, IRepositry<TEntity> repository, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
            _repository = repository;
            _membersToExpand = null;
        }
         public BaseManager(DbContext context, IRepositry<TEntity> repository, IMapper mapper, string[] membersToExpand)
        {
            _mapper = mapper;
            _context = context;
            _repository = repository;
            _membersToExpand = membersToExpand;
        }

        public async virtual Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>()
        {
            try
            {
                return await Task.Run(() => _repository.GetAll().ProjectTo<TProjectedModel>(_mapper.ConfigurationProvider, null, _membersToExpand).ToList());
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }
        public async Task<PagedResult<TViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            return await Task<PagedResult<TViewModel>>.Run(() =>
            {
                var pagedResult = new PagedResult<TViewModel>();
                if (pagingparametermodel.PageNumber == 0) pagingparametermodel.PageNumber = 1;
                var source = _repository.GetAll().ProjectTo<TViewModel>(_mapper.ConfigurationProvider, null, _membersToExpand);
                int currentPage = pagingparametermodel.PageNumber;
                int pageSize = pagingparametermodel.PageSize;
                pagedResult.TotalCount = source.Count();
                pagedResult.Result = source.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                return pagedResult;
            });
        }
        public async Task<PagedResult<TViewModel>> GetAllByPaginationAsync(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel)
        {
            return await Task<PagedResult<TViewModel>>.Run(() =>
            {
                var pagedResult = new PagedResult<TViewModel>();
                if (pagingparametermodel.PageNumber == 0) pagingparametermodel.PageNumber = 1;
                var source = listQuery.ProjectTo<TViewModel>(_mapper.ConfigurationProvider, null, _membersToExpand);
                int currentPage = pagingparametermodel.PageNumber;
                int pageSize = pagingparametermodel.PageSize;
                pagedResult.TotalCount = source.Count();//
                pagedResult.Result = source.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                return pagedResult;
            });

        }
        public async Task<PagedResult<TQueryModel>> GetAllByPaginationAsync<TQueryModel>(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel)
        {
            return await Task<PagedResult<TQueryModel>>.Run(() =>
            {
                var pagedResult = new PagedResult<TQueryModel>();
                if (pagingparametermodel.PageNumber == 0) pagingparametermodel.PageNumber = 1;
                var source = listQuery.ProjectTo<TQueryModel>(_mapper.ConfigurationProvider, null, _membersToExpand);
                int currentPage = pagingparametermodel.PageNumber;
                int pageSize = pagingparametermodel.PageSize;
                pagedResult.TotalCount = source.Count();//
                pagedResult.Result = source.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                return pagedResult;
            });
        }
        public async Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(bool ignoreTenant)
        {
            try
            {
                return await Task.Run(() => _repository.GetAll(ignoreTenant).ProjectTo<TProjectedModel>(_mapper.ConfigurationProvider).ToList());
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }
        public async Task<PagedResult<TViewModel>> GetAllByPaginationAsync(bool ignoreTenant, PaginatedItemsViewModel pagingParameterModel)
        {
            return await Task<PagedResult<TViewModel>>.Run(() =>
            {
                var pagedResult = new PagedResult<TViewModel>();
                if (pagingParameterModel.PageNumber == 0) pagingParameterModel.PageNumber = 1;
                var source = _repository.GetAll(ignoreTenant).ProjectTo<TViewModel>(_mapper.ConfigurationProvider);
                int currentPage = pagingParameterModel.PageNumber;
                int pageSize = pagingParameterModel.PageSize;
                pagedResult.TotalCount = source.Count();
                pagedResult.Result = source.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                return pagedResult;
            });
        }

        public async virtual Task<TViewModel> AddAsync(TViewModel viewModel)
        {
            return await Task<TViewModel>.Run(() =>
            {
                TEntity entity = _mapper.Map<TViewModel, TEntity>(viewModel);
                TEntity resultentity = _repository.AddAsync(entity);
                TViewModel result = _mapper.Map<TEntity, TViewModel>(resultentity);
                return result;
            });
        }
        public async virtual Task<List<TViewModel>> AddAsync(List<TViewModel> ViewModelList)
        {
            return await Task<List<TViewModel>>.Run(() =>
            {
                List<TEntity> entity = _mapper.Map<List<TViewModel>, List<TEntity>>(ViewModelList);
                List<TEntity> resultentity = _repository.Add(entity);
                List<TViewModel> result = _mapper.Map<List<TEntity>, List<TViewModel>>(resultentity);
                return result;
            });
        }

        public async virtual Task<bool> UpdateAsync(TViewModel viewModel)
        {
            return await Task<TViewModel>.Run(() =>
            {
                TEntity entity = _mapper.Map<TViewModel, TEntity>(viewModel);
                bool result = _repository.Update(entity);

                return result;
            });
        }
        public async virtual Task<bool> UpdateAsync<TCommand>(TCommand viewModel)
        {
            return await Task<TCommand>.Run(() =>
            {
                TEntity entity = _mapper.Map<TCommand, TEntity>(viewModel);
                bool result = _repository.Update(entity);

                return result;
            });
        }

        public async virtual Task<bool> DeleteAsync(TViewModel viewModel)
        {
            return await Task<List<TViewModel>>.Run(() =>
            {
                TEntity entity = _mapper.Map<TViewModel, TEntity>(viewModel);
                bool result = _repository.Delete(entity);

                return result;
            });
        }
        public async virtual Task<bool> DeleteAsync(List<TViewModel> ViewModelList)
        {
            return await Task<List<TViewModel>>.Run(() =>
            {
                List<TEntity> entity = _mapper.Map<List<TViewModel>, List<TEntity>>(ViewModelList);
                bool result = _repository.Delete(entity);
                return result;
            });
        }
        public async virtual Task<bool> DeleteByIdAsync(params object[] id)
        {
            return await Task<List<TViewModel>>.Run(() =>
            {
                bool result = _repository.DeleteById(id);
                return result;
            });
        }
        public async virtual Task<bool> SoftDeleteAsync(TViewModel viewModel)
        {
            return await Task<List<TViewModel>>.Run(() =>
            {
                TEntity entity = _mapper.Map<TViewModel, TEntity>(viewModel);
                bool result = _repository.SoftDelete(entity);
                return result;
            });
        }
    }
}
