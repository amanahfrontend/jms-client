﻿using JMS.DAL.Core.Persistence.Interfaces.IManagers;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace JMS.DAL.Core.Persistence.Implementation.Managers
{
    public class TenantService : ITenantService
    {
        private readonly HttpContext _httpContext;

        public TenantService(IHttpContextAccessor httpContext) => _httpContext = httpContext.HttpContext;
       
        public string GetCurrentTenantId()
        {
            if (_httpContext.User != null)
                if (_httpContext.User.Claims.Count() > 0) return _httpContext.User.Claims.Where(x => x.Type == "TenantId").FirstOrDefault().Value;

            return null;
        }
    }
}
