﻿using JMS.DAL.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace JMS.DAL.Core.EntityConfigurations
{
    public class BaseEntityConfiguration<T> : IEntityTypeConfiguration<T> where T : BaseEntity
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).ValueGeneratedOnAdd();
            builder.Property(e => e.RowId).HasValueGenerator<GuidValueGenerator>();
            builder.HasIndex(e => e.RowId);
            builder.Property(o => o.IsDeleted).IsRequired(); 
            builder.Property(o => o.CreatedDate).IsRequired();
            builder.Property(o => o.UpdatedDate).IsRequired();
            builder.Property(o => o.DeletedDate).IsRequired();
            builder.Property(o => o.TenantId).IsRequired(false);
            builder.Property(o => o.CreatedById).IsRequired(false);
            builder.Property(o => o.UpdatedById).IsRequired(false);
            builder.Property(o => o.DeletedById).IsRequired(false);
        }
    }
}
