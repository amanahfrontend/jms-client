﻿using JMS.DAL.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JMS.DAL.Core.EntityConfigurations
{
    public class BaseLocalizationConfiguration<T> : IEntityTypeConfiguration<T> where T : BaseLocalization
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).ValueGeneratedOnAdd();
        }
    }
}
