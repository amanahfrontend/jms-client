﻿namespace JMS.DAL.Core.Enums
{
    public enum SupportedLanguage : byte
    {
        English = 1,
        Arabic,
        Indian ,
        Urdu
    }
}