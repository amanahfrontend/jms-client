﻿using System;

namespace JMS.DAL.Core.IEntities
{
    public interface IBaseEntity
    {
        Guid RowId { get; set; }
        string CreatedById { get; set; }
        string UpdatedById { get; set; }
        string DeletedById { get; set; }
        string TenantId { set; get; }

        bool IsDeleted { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime UpdatedDate { get; set; }
        DateTime DeletedDate { get; set; }
    }
}
