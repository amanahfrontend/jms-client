﻿using JMS.DAL.Core.Enums;

namespace JMS.DAL.Core.IEntities
{
    public interface IBaseLocalization
    {
        int Id { get; set; }
        SupportedLanguage SupportedLanguage { get; set; }
    }
}
