﻿using JMS.DAL.Core.Enums;
using JMS.DAL.Core.IEntities;

namespace JMS.DAL.Core.Entities
{
    public class BaseLocalization :IBaseLocalization
    {
        public virtual int Id { get; set; }
        public virtual SupportedLanguage SupportedLanguage { get; set; }
    }
}
