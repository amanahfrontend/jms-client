﻿using JMS.DAL.Core.IEntities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JMS.DAL.Core.Entities
{
    public class BaseEntity : IBaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }
        public virtual Guid RowId { get; set; } = Guid.NewGuid();
        public virtual string CreatedById { get; set; }
        public virtual string UpdatedById { get; set; }
        public virtual string DeletedById { get; set; }
        public string TenantId { set; get; }

        public virtual bool IsDeleted { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime UpdatedDate { get; set; }
        public virtual DateTime DeletedDate { get; set; }
    }
}
