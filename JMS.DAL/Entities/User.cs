﻿using JMS.DAL.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JMS.DAL.Entities
{
    public class User : BaseEntity
    {
        [MaxLength(256)]
        public string FullName { get; set; }
        [Required]
        public string Username { get; set; }
        public string Email { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public bool IsActive { get; set; }
        public string ChangePasswordToken { get; set; }
        public string PhoneNumber { get; set; }
        public string EmergencyContact { get; set; }
        public string Nationality { get; set; }
        public string Address { get; set; }
        public string Comments { get; set; }
        public string Image { get; set; }
        public virtual List<UserAttachment> UserAttachments { get; set; }

        public int? UserGroupId { get; set; }
        [ForeignKey("UserGroupId")]
        public virtual UserGroup UserGroup { get; set; }
        public int? WorkForceId { get; set; }
        [ForeignKey("WorkForceId")]
        public virtual WorkForce WorkForce { get; set; }
        public int? RoleId { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }
      

        // [NotMapped]
        //public virtual List<UserRole> UserRoles { get; set; }
       


        //public virtual List<Notification> Notifications { get; set; }
        //public virtual List<Journey> Journeys { get; set; }

        //[InverseProperty("Dispatcher")]
        //public virtual List<Journey> DispatcherJourneys { get; set; }
        //public virtual List<JourneyUpdate> JourneyUpdates { get; set; }
        //public bool IsInProgress { get; set; }
        //public DateTime? LastCompletionTrip { get; set; }

    }
}
