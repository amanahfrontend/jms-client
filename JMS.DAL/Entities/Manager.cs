﻿using JMS.DAL.Core.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JMS.DAL.Entities
{
    public class Manager
    {
        [Key]
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public string Position { get; set; }
    }
}