﻿using JMS.DAL.Core.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JMS.DAL.Entities
{
    public class UserGroup: BaseEntity
    {
        [MaxLength(256)]
        [Required]
        public string Name { get; set; }

        public bool IsTeamType { get; set; }
        public string Description { get; set; }

        public virtual List<User> Users { get; set; }
    }
}
