﻿using JMS.DAL.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JMS.DAL.Entities
{
    public class Driver
    {
        [Key]
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [MaxLength(256)]
        public string LicenseNo { get; set; }
        public DateTime? LicenseExpiryDate { get; set; }
        public VehicleType DriveFor { get; set; }
        public string TrainingDetails { get; set; }
        public bool IsHavingChronicDiseases { get; set; }
        public string ChronicDiseases { get; set; }
        public string GatePassStatus { get; set; }

        public bool IsInProgress { get; set; }
        public DateTime? LastCompletionTrip { get; set; }
        [NotMapped]
        public virtual List<GatePassDetails> GatePassDetails { get; set; }
         

    }
}
