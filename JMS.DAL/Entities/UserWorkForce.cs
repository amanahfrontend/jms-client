﻿using JMS.DAL.Core.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JMS.DAL.Entities
{
    public class WorkForce: BaseEntity
    {
        [MaxLength(256)]
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<User> Users { get; set; }

    }
}
