﻿using JMS.DAL.Common.Enums;
using JMS.DAL.Core.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JMS.DAL.Entities
{
    public class Role : BaseEntity
    {
        [MaxLength(256)]
        [Required]
        public string Name { get; set; }
        public UserRoleType UserRoleType { get; set; }
        public virtual List<RolePermission> RolePermissions { get; set; }
    }
}
