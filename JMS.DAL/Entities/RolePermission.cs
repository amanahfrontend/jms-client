﻿using JMS.DAL.Common.Constant;
using JMS.DAL.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace JMS.DAL.Entities
{
    public class Permission
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public class FeaturePermission
    {
        public string Name { get; set; }
        public List<Permission> Permissions { get; set; }
    }

    public class RolePermission : BaseEntity
    {
        //public int RoleId { get; set; }
        //[ForeignKey("RoleId")]
        //public Role Role { get; set; }
        public string PermissionConstValue { get; set; }

        public static readonly IEnumerable<string> _journeyPermissions;
        public static readonly IEnumerable<string> _assessmentPermissions;
        public static readonly IEnumerable<string> _driverPermissions;
        public static readonly IEnumerable<string> _workforcePermissions;
        public static readonly IEnumerable<string> _teamPermissions;
        public static readonly IEnumerable<string> _settingettingsPermissions;

        static RolePermission()
        {
            Type journeyType = typeof(JourneyConstant);
            var journeyFlags = BindingFlags.Static | BindingFlags.Public;
            _journeyPermissions = journeyType.GetFields(journeyFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));

            Type assessmentType = typeof(AssessmentConstant);
            var assessmentFlags = BindingFlags.Static | BindingFlags.Public;
            _assessmentPermissions = assessmentType.GetFields(assessmentFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));

            Type driverType = typeof(DriverConstant);
            var driverFlags = BindingFlags.Static | BindingFlags.Public;
            _driverPermissions = driverType.GetFields(driverFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));

            Type workforceType = typeof(WorkForceConstant);
            var workforceFlags = BindingFlags.Static | BindingFlags.Public;
            _workforcePermissions = workforceType.GetFields(workforceFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));


            Type teamType = typeof(TeamConstant);
            var teamFlags = BindingFlags.Static | BindingFlags.Public;
            _teamPermissions = teamType.GetFields(teamFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));


            Type settingsType = typeof(SettingsConstant);
            var settingsFlags = BindingFlags.Static | BindingFlags.Public;
            _settingettingsPermissions = settingsType.GetFields(settingsFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));
        }

        public async Task<List<FeaturePermission>> GetManagerPermissions()
        {
            List<FeaturePermission> features = new List<FeaturePermission>();

            FeaturePermission journeyPermission = new FeaturePermission
            {
                Name = "Journey Permissions",
                Permissions = new List<Permission>
                {
                    new Permission() { Name = "Create Journey ", Value = JourneyConstant.Create },
                    new Permission() { Name = "Update Journey ", Value = JourneyConstant.Update },
                    new Permission() { Name = "Change Journey Status ", Value =JourneyConstant.ChangeStatus },
                    new Permission() { Name = "Change Journey Driver ", Value =JourneyConstant.ChangeDriver },
                    new Permission() { Name = "View My Requests ", Value =  JourneyConstant.BrowseMyRequest },
                    new Permission() { Name = "Approve or Reject Journey ", Value =  JourneyConstant.ApproveOrReject },
                }
            };
            FeaturePermission assenmentPermission = new FeaturePermission
            {
                Name = "Assessment Permissions",
                Permissions = new List<Permission>
                {
                    new Permission() { Name = "Create Journey Assessment ", Value = AssessmentConstant.Create },
                    new Permission() { Name = "Update Journey Assessment ", Value = AssessmentConstant.Update },
                    new Permission() { Name = "Delete Journey Assessment ", Value = AssessmentConstant.Delete },
                    new Permission() { Name = "Read Journey Assessment ", Value = AssessmentConstant.Browse },
                }
            };
            FeaturePermission driverPermission = new FeaturePermission
            {
                Name = "Driver Permissions",
                Permissions = new List<Permission>
                {
                new Permission() { Name = "Create Driver ", Value =  DriverConstant.Create },
                new Permission() { Name = "Update Driver ", Value =  DriverConstant.Update },
                new Permission() { Name = "Delete Driver ", Value =  DriverConstant.Delete },
                new Permission() { Name = "Delete All Driver ", Value =  DriverConstant.DeleteAll },
                }
            };
            FeaturePermission workforsePermission = new FeaturePermission
            {
                Name = "Workforse Permissions",
                Permissions = new List<Permission>
                {
                new Permission() { Name = "Create Workforse ", Value =  WorkForceConstant.Create },
                new Permission() { Name = "Update Workforse ", Value =  WorkForceConstant.Update },
                new Permission() { Name = "Delete Workforse ", Value =  WorkForceConstant.Delete },
                new Permission() { Name = "Read Workforses ", Value =  WorkForceConstant.Browse }
                }
            };
            FeaturePermission teamPermission = new FeaturePermission
            {
                Name = "Team Permissions",
                Permissions = new List<Permission>
                {
                new Permission() { Name = "Create Team ", Value =  TeamConstant.Create },
                new Permission() { Name = "Update Team ", Value =  TeamConstant.Update },
                new Permission() { Name = "Delete  Team ", Value =  TeamConstant.Delete },
                new Permission() { Name = "Read Teams ", Value =  TeamConstant.Browse }
                }
            };
            FeaturePermission settingsPermission = new FeaturePermission
            {
                Name = "Settings Permissions",
                Permissions = new List<Permission>
                {
                    new Permission() { Name = "Change My Password ", Value =  SettingsConstant.ChangeMyPassword },
                    new Permission() { Name = "Manage My Profile ", Value =  SettingsConstant.ManageMyProfile },
                    new Permission() { Name = "Read Notification ", Value =  SettingsConstant.ReadNotification },
                    new Permission() { Name = "Update Notification ", Value =  SettingsConstant.UpdateNotification }
                }
            };

            features.Add(journeyPermission);
            features.Add(assenmentPermission);
            features.Add(driverPermission);
            features.Add(workforsePermission);
            features.Add(teamPermission);
            features.Add(settingsPermission);

            return features;
        }
        public async Task<List<FeaturePermission>> GetDriverPermissions()
        {
            List<FeaturePermission> features = new List<FeaturePermission>();

            FeaturePermission journeyPermission = new FeaturePermission
            {
                Name = "Journey Permissions",
                Permissions = new List<Permission>
                {
                    new Permission() { Name = "Start Journey ", Value = JourneyConstant.Start },
                }
            };
            FeaturePermission assenmentPermission = new FeaturePermission
            {
                Name = "Assessment Permissions",
                Permissions = new List<Permission>
                {
                    new Permission() { Name = "Answer Journey Assessment ", Value = AssessmentConstant.Answer },
                }
            };
            FeaturePermission settingsPermission = new FeaturePermission
            {
                Name = "Settings Permissions",
                Permissions = new List<Permission>
                {
                    new Permission() { Name = "Change My Password ", Value =  SettingsConstant.ChangeMyPassword },
                    new Permission() { Name = "Manage My Password ", Value =  SettingsConstant.ManageMyProfile },
                    new Permission() { Name = "Read Notification ", Value =  SettingsConstant.ReadNotification },
                    new Permission() { Name = "Update Notification ", Value =  SettingsConstant.UpdateNotification }
                }
            };

            features.Add(assenmentPermission);
            features.Add(journeyPermission);
            features.Add(settingsPermission);
            return features;
        }

        public static List<RolePermission> GetAdminRolePermissions()
        {
            return new List<RolePermission>
            {
                new RolePermission{ PermissionConstValue=DriverConstant.Create },
                new RolePermission{ PermissionConstValue=DriverConstant.Update },
                new RolePermission{ PermissionConstValue=DriverConstant.Browse },
                new RolePermission{ PermissionConstValue=DriverConstant.Delete },
                new RolePermission{ PermissionConstValue=DriverConstant.DeleteAll },
                new RolePermission{ PermissionConstValue=ManagerConstant.Create },
                new RolePermission{ PermissionConstValue=ManagerConstant.Update },
                new RolePermission{ PermissionConstValue=ManagerConstant.Browse },
                new RolePermission{ PermissionConstValue=ManagerConstant.Delete },
                new RolePermission{ PermissionConstValue=ManagerConstant.DeleteAll },
                new RolePermission{ PermissionConstValue=WorkForceConstant.Create },
                new RolePermission{ PermissionConstValue=WorkForceConstant.Update },
                new RolePermission{ PermissionConstValue=WorkForceConstant.Browse },
                new RolePermission{ PermissionConstValue=WorkForceConstant.Delete },
                new RolePermission{ PermissionConstValue=TeamConstant.Create },
                new RolePermission{ PermissionConstValue=TeamConstant.Update },
                new RolePermission{ PermissionConstValue=TeamConstant.Browse },
                new RolePermission{ PermissionConstValue=TeamConstant.Delete },
                new RolePermission{ PermissionConstValue=RoleConstant.Create },
                new RolePermission{ PermissionConstValue=RoleConstant.Update },
                new RolePermission{ PermissionConstValue=RoleConstant.Browse },
                new RolePermission{ PermissionConstValue=RoleConstant.Delete },
                new RolePermission{ PermissionConstValue=SettingsConstant.ChangeMyPassword  },
                new RolePermission{ PermissionConstValue=SettingsConstant.ManageMyProfile },
            };
        }

        public static List<RolePermission> GetDriverRolePermissions()
        {
            return new List<RolePermission>
            {
                new RolePermission{ PermissionConstValue=JourneyConstant.Start },
                new RolePermission{ PermissionConstValue=AssessmentConstant.Answer },
                new RolePermission{ PermissionConstValue=SettingsConstant.UpdateNotification  },
                new RolePermission{ PermissionConstValue=SettingsConstant.ReadNotification },
                new RolePermission{ PermissionConstValue=SettingsConstant.ChangeMyPassword  },
                new RolePermission{ PermissionConstValue=SettingsConstant.ManageMyProfile },
            };
        }

    }
}
