﻿using JMS.DAL.Core.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace JMS.DAL.Entities
{
    public class UserAttachment : BaseEntity
    {
        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public string Attachment { get; set; }
    }
}
