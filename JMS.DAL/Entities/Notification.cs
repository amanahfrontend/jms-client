﻿using JMS.DAL.Common.Enums;
using System;

namespace JMS.DAL.Entities
{
    public class Notification
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsRead { get; set; }
        public DateTime CreationTime { get; set; }
        public Guid? UserId { get; set; }
        public virtual User User { get; set; }
        public NotificationType? NotificationType { get; set; }
        public UserRoleType? Role { get; set; }
        public int? JourneyId { get; set; }
    }
    public enum NotificationType
    {
        JourneyInfo,
    }
}
