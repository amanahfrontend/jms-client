﻿namespace JMS.DAL.Common.Constant
{
    public class Constant
    {
        public const string CONNECTION_STRING_CONFIG_NAME= "JMSDBConnection";
    }

    public static class JourneyConstant
    {
        public const string Create = "Journey.Create";
        public const string CreateUnassigned = "Journey.CreateUnassigned";
        public const string Update = "Journey.Update";
        public const string ChangeStatus = "Journey.ChangeStatus";
        public const string ReadUnassigned = "Journey.ReadUnassigned";
        public const string ChangeDriver = "Journey.ChangeDriver";
        public const string BrowseMyRequest = "Journey.BrowseMyRewquest";
        public const string ApproveOrReject = "Journey.ApproveOrReject";
        public const string Start = "Journey.Start";
    }
    public static class AssessmentConstant
    {
        public const string Create = "Assessment.Create";
        public const string Update = "Assessment.Update";
        public const string Delete = "Assessment.Delete";
        public const string DeleteAll = "Assessment.DeleteAll";
        public const string Browse = "Assessment.Browse";
        public const string Answer = "Assessment.Answer";
    }
    public static class DriverConstant
    {
        public const string Create = "Driver.Create";
        public const string Update = "Driver.Update";
        public const string Delete = "Driver.Delete";
        public const string DeleteAll = "Driver.DeleteAll";
        public const string Browse = "Driver.Browse";
    }   
   
   public static class ManagerConstant
    {
        public const string Create = "Manager.Create";
        public const string Update = "Manager.Update";
        public const string Delete = "Manager.Delete";
        public const string DeleteAll = "Manager.DeleteAll";
        public const string Browse = "Manager.Browse";

    }  
    public static class RoleConstant
    {
        public const string Create = "Role.Create";
        public const string Update = "Role.Update";
        public const string Delete = "Role.Delete";
        public const string DeleteAll = "Role.DeleteAll";
        public const string Browse = "Role.Browse";

    }
    public static class WorkForceConstant
    {
        public const string Create = "WorkForce.Create";
        public const string Delete = "WorkForce.Delete";
        public const string Update = "WorkForce.Update";
        public const string Browse = "WorkForce.Browse";

    }
    public static class TeamConstant
    {
        public const string Create = "Team.Create";
        public const string Delete = "Team.Delete";
        public const string Update = "Team.Update";
        public const string Browse = "Team.Browse";
        public const string DeleteAll = "Team.DeleteAll";
    }
    public static class SettingsConstant
    {
        public const string ReadAdvancePreference = "Settings.ReadAdvancePreference";
        public const string UpdateAdvancedPreference = "Settings.UpdateAdvancedPreference";
        public const string ReadAutoAllocation = "Settings.ReadAutoAllocation";
        public const string UpdateAutoAllocation = "Settings.UpdateAutoAllocation";
        public const string ReadGeofence = "Settings.ReadGeofence";
        public const string UpdateGeofence = "Settings.UpdateGeofence";
        public const string ReadNotification = "Settings.ReadNotification";
        public const string UpdateNotification = "Settings.UpdateNotification";
        public const string ManageMyProfile = "Settings.ManageMyProfile";
        public const string ChangeMyPassword = "Settings.ChangeMyPassword";
    }


}
