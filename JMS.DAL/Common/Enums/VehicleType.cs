﻿namespace JMS.DAL.Enums
{
    public enum VehicleType : byte
    {
        Light,
        Heavy
    }
}
