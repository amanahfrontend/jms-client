﻿namespace JMS.DAL.Common.Enums
{
    public enum QuestionCategory
    {
        CheckpointAssessment = 0,
        VehicleChecklist = 1,
        PreTrip = 2,
        PostTrip = 3
    }

}
