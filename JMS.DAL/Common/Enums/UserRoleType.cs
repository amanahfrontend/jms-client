﻿namespace JMS.DAL.Common.Enums
{
    public enum UserRoleType :byte
    {
        ProductLine = 0,
        Dispatcher = 1,
        JMC = 2,
        Driver = 3,
        QHSE = 4,
        GBM = 5,
        OperationManager = 6  ,
        Admin = 7,
    }
}
