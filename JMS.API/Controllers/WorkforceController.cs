﻿using AutoMapper;
using JMS.API.Constants;
using JMS.BLL.Common;
using JMS.BLL.Helper;
using JMS.BLL.Interfaces;
using JMS.BLL.ViewModels;
using JMS.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace JMS.API.Controllers
{
  //  [Authorize(Roles = ConstUserRoleType.JMSAdmin)]
    [ApiController]
    [Route("api/workforce")]
    public class WorkforceController : ControllerBase
    {
        private readonly IUserService _userService;

        public WorkforceController(IUserService userService) => _userService = userService;

        [HttpGet]
        public IActionResult GetAll() => Ok(_userService.GetWorkForces());

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id) => Ok(_userService.GetWorkForce(id));

        [HttpPost]
        [Route("Add")]
        public IActionResult Add(WorkForce workforce) => Ok(_userService.AddWorkForce(workforce));
       
        [HttpPost]
        [Route("Update")]
        public IActionResult Update(WorkForce workforce) => Ok(_userService.EditWorkForce(workforce));

        [HttpDelete]
        [Route("Delete")]
        public IActionResult Delete(Guid workforceId)
        {
            return Ok(_userService.DeleteWorkForce(workforceId));
        }

    }
}