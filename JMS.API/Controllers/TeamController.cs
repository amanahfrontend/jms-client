﻿using AutoMapper;
using JMS.API.Constants;
using JMS.BLL.Common;
using JMS.BLL.Helper;
using JMS.BLL.Interfaces;
using JMS.BLL.Services;
using JMS.BLL.ViewModels;
using JMS.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace JMS.API.Controllers
{
    [ApiController]
  //  [Authorize(Roles = ConstUserRoleType.JMSAdmin)]
    [Route("api/team")]
    public class TeamController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public TeamController(IUserService userService, IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpGet]
        public IActionResult GetAll() => Ok(_userService.GetUserGroups());

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            try
            {
                UserGroup userGroup = _userService.GetUserGroupById(id);
                var result = new ServiceResponse<UserGroup> { Data = userGroup, Status = JMS.DAL.Common.Enums.ResponseStatus.Success };
                return Ok(result);
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError, Message = ex.Message });
            }
        }

        [HttpPost]
        [Route("Add")]
        public IActionResult Add(UserGroup group) => Ok(_userService.AddUserGroup(group));

        [HttpPost]
        [Route("Update")]
        public IActionResult Update(UserGroup group) => Ok(_userService.EditUserGroup(group));

        [HttpPost]
        [Route("Delete/{rowId}")]
        public IActionResult Delete(Guid rowId)
        {
            return Ok(_userService.DeleteUserGroup(rowId));
        }
    }
}