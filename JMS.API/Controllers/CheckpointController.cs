﻿using AutoMapper;
using JMS.API.Constants;
using JMS.BLL.Common;
using JMS.BLL.Helper;
using JMS.BLL.Interfaces;
using JMS.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;

namespace JMS.API.Controllers
{

    [Authorize]
    [ApiController]
    [Route("api/checkpoint")]
    public class CheckpointController : ControllerBase
    {
        private readonly ICheckpointService _checkpointService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public CheckpointController(ICheckpointService checkpointService, IOptions<AppSettings> appSettings)
        {
            _checkpointService = checkpointService;
            _appSettings = appSettings.Value;
        }


        [HttpGet]     //  ("{fromLat, fromLng, toLat, toLng, isThirdParty}")
        [Authorize(Roles = "Product Line,Dispatcher,JMC")]
        [Route("getcheckpoints")] 
        public IActionResult GetCheckpoints(double fromLat, double fromLng, double toLat, double toLng, bool isThirdParty = false)
        {
            return Ok(_checkpointService.GetCheckpoints(fromLat, fromLng, toLat, toLng, isThirdParty));
        }

        [HttpPost]
        [Route("addcheckpoint")]
        [Authorize(Roles = ConstUserRoleType.JMSAdmin)]
        public IActionResult AddCheckpoint(Checkpoint checkpoint)
        {
            return Ok(_checkpointService.AddCheckpoint(checkpoint));
        }

        [HttpPost]
        [Authorize(Roles = ConstUserRoleType.JMSAdmin)]
        [Route("updatecheckpoint")]
        public IActionResult UpdateCheckpoint(Checkpoint checkpoint)
        {
            return Ok(_checkpointService.UpdateCheckpoint(checkpoint));
        }

        [HttpPost]
        [Authorize(Roles = ConstUserRoleType.JMSAdmin)]
        [Route("deletecheckpoint")]
        public IActionResult DeleteCheckpoint(int checkpointId)
        {
            return Ok(_checkpointService.DeleteCheckpoint(checkpointId));
        }

        [HttpGet]
        [Authorize(Roles = ConstUserRoleType.Driver)]
        [Route("getCheckpointsByJourneyId")]
        public IActionResult GetCheckpointsByJourneyId(int journeyId)
        {
            try
            {
                return Ok(_checkpointService.GetCheckpointsByJourneyId(journeyId));
            }
            catch (Exception ex)
            {
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError, Exception = ex.ToString() });
            }
        }
    }



}