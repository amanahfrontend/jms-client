﻿using AutoMapper;
using JMS.API.Constants;
using JMS.BLL.Common;
using JMS.BLL.Helper;
using JMS.BLL.Interfaces;
using JMS.BLL.ViewModels;
using JMS.DAL.Common.Enums;
using JMS.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JMS.API.Controllers
{
    // [Authorize(Roles = "Dispatcher,JMC,JMS Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class ManagerController : ControllerBase
    {
        private readonly IManagerService _managerService;
        private readonly IUserService _userService;
        private readonly IJourneyService _journeyService;
        private readonly IMapper _mapper;
        private List<JourneyStatus> _endedJourneyStatuses = new List<JourneyStatus> { JourneyStatus.JourneyClosed, JourneyStatus.JourneyCompleted, JourneyStatus.JourneyRejected };

        public ManagerController(IManagerService managerService, IUserService userService, IMapper mapper)
        {
            _managerService = managerService;
            _userService = userService;
            _mapper = mapper;
        }
        public ManagerController(IManagerService managerService, IUserService userService, IJourneyService journeyService, IMapper mapper)
        {
            _managerService = managerService;
            _userService = userService;
            _journeyService = journeyService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("GetManagers")]
        public IActionResult GetManagers(string driverName = "") => Ok(_managerService.GetManagers(driverName));

        [HttpGet("{rowId}")]
        public IActionResult GetById(Guid rowId) => Ok(_managerService.GetFirstOrDefaultBy(e => e.User.RowId == rowId));

        [HttpPost]
        [Route("Add")]
        public IActionResult Add(ManagerModel managerModel)
        {
            try
            {
                var user = _mapper.Map<User>(managerModel);
                Manager manager = _mapper.Map<Manager>(managerModel);
                User insertedUser = _userService.Add(user, managerModel.Password);
                manager.UserId = insertedUser.Id;
                Manager insertedDriver = _managerService.Create(manager);
                return Ok(new ServiceResponse { Status = ResponseStatus.Success });
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = ResponseStatus.ServerError, Message = ex.Message });
            }
        }

        [HttpPost]
        [Route("Update")]
        public IActionResult Update(ManagerModel managerModel)
        {
            try
            {
                var user = _mapper.Map<User>(managerModel);
                Manager manager = _mapper.Map<Manager>(managerModel);
                if (!user.IsActive && ValidateManagerActiveJourneys(manager.UserId)) return BadRequest(new { message = "Manager has an active Journey" });
                _userService.Update(user, managerModel.Password);
                Manager updatedDriver = _managerService.Update(manager);
                return Ok(new ServiceResponse { Status = ResponseStatus.Success });
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = ResponseStatus.ServerError, Message = ex.Message });
            }
        }

        [HttpPost]
        [Route("Delete {rowId}")]
        public IActionResult Delete(Guid rowId)
        {
            try
            {
                Manager manager = _managerService.GetFirstOrDefaultBy(e => e.User.RowId == rowId);
                if (ValidateManagerActiveJourneys(manager.UserId)) return BadRequest(new { message = "Manager has an active Journey" });
                _userService.Delete(rowId);
                _managerService.Delete(manager);
                return Ok(new ServiceResponse { Status = ResponseStatus.Success });
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = ResponseStatus.ServerError, Message = ex.Message });
            }
        }
        [HttpPost]
        [Route("SoftDelete {rowId}")]
        public IActionResult SoftDelete(Guid rowId)
        {
            try
            {
                Manager manager = _managerService.GetFirstOrDefaultBy(e => e.User.RowId == rowId);
                if (ValidateManagerActiveJourneys(manager.UserId)) return BadRequest(new { message = "Manager has an active Journey" });
                manager.User.IsDeleted = true;
                _userService.Update(manager.User);
                return Ok(new ServiceResponse { Status = ResponseStatus.Success });
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = ResponseStatus.ServerError, Message = ex.Message });
            }
        }

        [HttpPost]
        [Route("ActivateDeactivate")]
        public IActionResult ActivateDeactivate(Guid rowId, bool isActive)
        {
            try
            {
                if (!isActive)
                {
                    Manager manager = _managerService.GetFirstOrDefaultBy(e => e.User.RowId == rowId);
                    if (ValidateManagerActiveJourneys(manager.UserId)) return BadRequest(new { message = "Manager has an active Journey" });
                }
                _userService.ActivateDisactvate(rowId, isActive);
                return Ok(new ServiceResponse { Status = ResponseStatus.Success });
            }
            catch (Exception ex)
            {
                ex.LogException();
                return Ok(new ServiceResponse { Status = ResponseStatus.ServerError });
            }
        }


        private List<KeyValuePair<int, string>> GetManagerActiveJourneys(int driverId)
        {
            var journeys = _journeyService.GetBy(e => e.User.Id == driverId && _endedJourneyStatuses.Any(s => s != e.JourneyStatus));
            return journeys != null && journeys.Count > 0 ? journeys.Select(e => new KeyValuePair<int, string>(e.Id, e.Title)).ToList() : null;
        }
        private bool ValidateManagerActiveJourneys(int driverId) => _journeyService.ValidateBy(e => e.CurrentDriverId == driverId && _endedJourneyStatuses.Any(s => s != e.JourneyStatus));

    }
}
