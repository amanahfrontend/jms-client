﻿using AutoMapper;
using JMS.API.Constants;
using JMS.BLL.Interfaces;
using JMS.BLL.ViewModels;
using JMS.DAL.Common.Enums;
using JMS.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace JMS.API.Controllers
{
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;
        private readonly IMapper _mapper;

        public RoleController(IRoleService roleService, IMapper mapper)
        {
            _roleService = roleService;
            _mapper = mapper;
        }

        [Route("GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            List<Role> entityResult = _roleService.GetAllRoles();
            var result = _mapper.Map<List<RoleModel>>(entityResult);
            return Ok(result);
        }

        [Route("Get")]
        [HttpGet]
        public IActionResult Get(Guid rowId)
        {
            Role role = _roleService.GetRoleBy(e => e.RowId == rowId);
            return Ok(role);
        }


        [Route("Add")]
        [HttpPost]
        public IActionResult Post(RoleModel model)
        {
            Role entityResult = _mapper.Map<Role>(model);
            entityResult = _roleService.AddRole(entityResult);
            return Ok(entityResult);
        }

        [Route("Update")]
        [HttpPost]
        public IActionResult Put(RoleModel model)
        {
            Role entityResult = _mapper.Map<Role>(model);
            bool result = _roleService.UpdateRole(entityResult);
            return Ok(result);
        }

        [Route("Delete/{rowId}")]
        [HttpDelete]
        public IActionResult Delete(Guid rowId)
        {
            Role entity = _roleService.GetRoleBy(e => e.RowId == rowId);
            bool result = _roleService.DeleteRole(entity);
            return Ok(result);
        }

        [HttpGet]
        public IActionResult Init()
        {
            Role adminRole = new Role
            {
                Name = ConstUserRoleType.JMSAdmin,
                UserRoleType = UserRoleType.Admin,
                RolePermissions = RolePermission.GetAdminRolePermissions()
            };
            Role driverRole = new Role
            {
                Name = ConstUserRoleType.Driver,
                UserRoleType = UserRoleType.Driver,
                RolePermissions = RolePermission.GetDriverRolePermissions()
            };
            _roleService.AddRole(adminRole);
            _roleService.AddRole(driverRole);
            return Ok();
        }


    }
}