﻿using AutoMapper;
using JMS.BLL.Interfaces;
using JMS.DAL.Common.Enums;
using JMS.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace JMS.API.Controllers
{
  //  [Authorize]
    [ApiController]
    [Route("api/driver")]
    public partial class DriverController : ControllerBase
    {
        private readonly IDriverService _driverService;
        private readonly IUserService _userService;
        private readonly IJourneyService _journeyService;
        private readonly IMapper _mapper;
        private List<JourneyStatus> _endedJourneyStatuses = new List<JourneyStatus> { JourneyStatus.JourneyClosed, JourneyStatus.JourneyCompleted, JourneyStatus.JourneyRejected };

        public DriverController(IDriverService driverService, IUserService userService,IJourneyService journeyService, IMapper mapper)
        {
            _driverService = driverService;
            _userService = userService;
            _journeyService = journeyService;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize(Roles = Constants.ConstUserRoleType.Driver)]
        [Route("GetPreTripAssessment")]
        public IActionResult GetPreJourneyAssessment(int journeyId) => Ok(_driverService.GetJourneyAssessment(journeyId, false, false));

        [HttpGet]
        [Authorize(Roles = Constants.ConstUserRoleType.Driver)]
        [Route("GetPostTripAassessment")]
        public IActionResult GetPostJourneyAssessment(int journeyId) => Ok(_driverService.GetJourneyAssessment(journeyId, true, false));

        [HttpGet]
        [Authorize(Roles = Constants.ConstUserRoleType.Driver)]
        [Route("GetCheckPointAssessment/{checkpointid}/{journeyId}")]
        public IActionResult GetJourneyAssessment(int checkpointid, int journeyId) => Ok(_driverService.GetCheckpointAssessment(checkpointid, journeyId, false));

        [HttpGet()]
        [Authorize(Roles = Constants.ConstUserRoleType.Driver)]
        [Route("GetPreTripAssessmentResult/{journeyid}")]
        public IActionResult GetPreJourneyAssessmentResult(int journeyId) => Ok(_driverService.GetJourneyAssessment(journeyId, false, true));

        [HttpGet()]
        [Authorize(Roles = Constants.ConstUserRoleType.Driver)]
        [Route("getposttripassessmentresult/{journeyid}")]
        public IActionResult GetPostJourneyAssessmentResult(int journeyId) => Ok(_driverService.GetJourneyAssessment(journeyId, true, true));

        [HttpGet()]
        [Authorize(Roles = Constants.ConstUserRoleType.Driver)]
        [Route("GetCheckpointAssessmentResult/{checkpointid}/{journeyId}")]
        public IActionResult GetCheckpointAssessmentResult(int checkpointid, int journeyId) => Ok(_driverService.GetCheckpointAssessment(checkpointid, journeyId, true));

        [HttpPost]
        [Authorize(Roles = Constants.ConstUserRoleType.Driver)]
        [Route("submitassessment")]
        public IActionResult SubmitAssessment(int journeyId, int? ju, JourneyStatus status, List<AssessmentResult> assessmentResult)
        {
            return Ok(_driverService.SubmitAssessment(journeyId, ju, status, assessmentResult));
        }

        [HttpPost]
        [Authorize(Roles = Constants.ConstUserRoleType.Driver)]
        [Route("submitstatus")]
        public IActionResult SubmitStatus(JourneyUpdate status)
        {
            return Ok(_driverService.SubmitStatus(status));
        }
    }
}