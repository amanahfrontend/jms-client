﻿using AutoMapper;
using JMS.BLL.Helper;
using JMS.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace JMS.API.Controllers
{
   // [Authorize]
    [ApiController]
    [Route("api/Common")]
    public class CommonController : ControllerBase
    {
        private readonly ICommonService _commonService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public CommonController(ICommonService commonService, IOptions<AppSettings> appSettings)
        {
            _commonService = commonService;
            _appSettings = appSettings.Value;
        }
    }
}
