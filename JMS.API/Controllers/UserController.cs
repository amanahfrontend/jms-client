﻿using AutoMapper;
using JMS.API.Constants;
using JMS.BLL.Common;
using JMS.BLL.Helper;
using JMS.BLL.Interfaces;
using JMS.BLL.ViewModels;
using JMS.DAL.Common.Enums;
using JMS.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace JMS.API.Controllers
{
    //  [Authorize]
    [ApiController]
    [Route("api/user")]
    public partial class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UsersController(IUserService userService, IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [Authorize(Roles = ConstUserRoleType.JMSAdmin)]
        [HttpGet]
        [Route("GetAllUsers")]
        public IActionResult GetAllUsers()
        {
            try
            {
                return Ok(_userService.GetAllUsers());
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse
                {
                    Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError,
                    Message = ex.Message
                });
            }
        }

        [HttpGet]
        [Authorize(Roles = ConstUserRoleType.JMSAdmin)]
        public IActionResult GetAll(string keywordfilter, PagingProperties pagingProperties)
        {
            try
            {
                var users = _userService.GetAll(keywordfilter, pagingProperties);
                var result = new ServiceResponse<PageResult<User>> { Data = users, Status = JMS.DAL.Common.Enums.ResponseStatus.Success };
                return Ok(result);
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError, Message = ex.Message });
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            try
            {
                var user = _userService.GetUserById(id);
                var result = new ServiceResponse<UserModel> { Data = _mapper.Map<UserModel>(user), Status = JMS.DAL.Common.Enums.ResponseStatus.Success };
                return Ok(result);
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError, Message = ex.Message });
            }
        }

        [HttpGet]
        [Authorize(Roles = ConstUserRoleType.ProductLine)]
        [Route("GetDispatchers")]
        public IActionResult GetDispatchers() => Ok(_userService.GetUsersByRole(JMS.DAL.Common.Enums.UserRoleType.Dispatcher));

        [AllowAnonymous]
        [Route("Authenticate")]
        [HttpPost]
        public IActionResult Authenticate(AuthenticateModel model)
        {
            var user = _userService.Authenticate(model.Username.ToLower(), model.Password);
            if (user == null) return BadRequest(new { message = "Username or password is incorrect" });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var claims = new List<Claim> { new Claim(ClaimTypes.Name, user.RowId.ToString()) };
            var userRole = user.Role;
            Role role;
            if (userRole == null) role = new Role { Name = "JMS Admin", UserRoleType = UserRoleType.Admin };
            else role = _userService.GetRoleById(userRole.Id);


            claims.Add(new Claim(ClaimTypes.Role, role.Name));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            var result = new UserModel
            {
                Id=user.Id,
                RowId = user.RowId,
                Username = user.Username,
                FullName = user.FullName,
                Token = tokenString,
                Role = _mapper.Map<RoleModel>(role)
            };
            return Ok(new ServiceResponse<UserModel> { Data = result, Status = DAL.Common.Enums.ResponseStatus.Success });
        }


        //[Authorize(Roles = ConstUserRoleType.JMSAdmin)]
        [Route("Register")]
        [HttpPost]
        public IActionResult AddUser(RegisterModel model)
        {
            var user = new User
            {
                FullName = model.FullName,
                Email = model.Email,
                Username = model.Username.ToLower(),
                IsActive = true,
                // UserRoles = new List<UserRole> { new UserRole { RoleId = model.RoleId } },
            };
            try
            {
                _userService.Add(user, model.Password);
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.Success });
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError, Message = ex.Message });
            }
        }


        [Authorize(Roles = ConstUserRoleType.JMSAdmin)]
        [HttpPut]
        public IActionResult Update(RegisterModel model)
        {
            try
            {
                User user = _mapper.Map<User>(model);
                _userService.Update(user, model.Password);
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.Success });
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError, Message = ex.Message });
            }
        }

        [Authorize(Roles = ConstUserRoleType.JMSAdmin)]
        [HttpPost]
        [Route("ActivateDisActivate")]
        public IActionResult ActivateDisActivate(Guid id, bool isActive)
        {
            try
            {
                _userService.ActivateDisactvate(id, isActive);
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.Success });
            }
            catch (Exception ex)
            {
                ex.LogException();
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError });
            }
        }

        [HttpPost]
        [Route("ChangePassword")]
        public IActionResult ChangePassword(ChangePasswordModel model)
        {
            try
            {
                var userid = Guid.Parse(User.Identity.Name);
                var result = _userService.ChangePassword(userid, model.OldPassword, model.NewPassword);
                return Ok(result);
            }
            catch (Exception ex)
            {
                ex.LogException();
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError });
            }
        }

        [HttpPost]
        [Route("ResetPassword")]
        public IActionResult ResetPassword(Guid userid)
        {
            try
            {
                var password = General.CreatePassword(8);
                _userService.ResetPassword(userid, password);
                return Ok(new ServiceResponse<string> { Data = password, Status = JMS.DAL.Common.Enums.ResponseStatus.Success });
            }
            catch (Exception ex)
            {
                ex.LogException();
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ForgetPassword")]
        public IActionResult ForgetPassword(string username)
        {
            try
            {
                return Ok(_userService.ForgetPassword(username, _appSettings.Email, _appSettings.Password, _appSettings.WebUrl));
            }
            catch (Exception ex)
            {
                ex.LogException();
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ResetForgetPassword")]
        public IActionResult ResetForgetPassword(string token, string newPassword)
        {
            try
            {
                return Ok(_userService.ResetForgetPassword(token, newPassword));
            }
            catch (Exception ex)
            {
                ex.LogException();
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError });
            }
        }

        [Authorize(Roles = ConstUserRoleType.JMSAdmin)]
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                _userService.Delete(id);
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.Success });
            }
            catch (AppException ex)
            {
                ex.LogException();
                // return error message if there was an exception
                return BadRequest(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError, Message = ex.Message });
            }

        }

    }
}