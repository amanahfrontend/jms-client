﻿using JMS.BLL.Common;
using JMS.BLL.Interfaces;
using JMS.BLL.ViewModels;
using JMS.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JMS.API.Controllers
{

    [Authorize]
    [ApiController]
    [Route("api/notification")]
    public class NotificationController : ControllerBase
    {
        private INotificationService _notificationService;

        public NotificationController(INotificationService notificationService)
        {            
            _notificationService = notificationService;
        }

        [HttpGet()]
        [Route("getusernotifications")]
        public IActionResult GetUserNotifications()
        {
            try
            {
                var userId = Guid.Parse(User.Identity.Name);
                var model = _notificationService.GetUserNotifications(userId);
                var notifications = model.Select(x => new NotificationModel
                {
                    CreationTime = x.CreationTime,
                    Id = x.Id,
                    IsRead = x.IsRead,
                    Text = x.Text,
                    UserId = x.UserId
                }).ToList();
                return Ok(new ServiceResponse<List<NotificationModel>>
                {
                    Data = notifications,
                    Status = JMS.DAL.Common.Enums.ResponseStatus.Success
                });
            }
            catch (Exception ex)
            {
                ex.LogException();
                return Ok(new ServiceResponse
                {
                    Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError
                });

            }
        }

        [HttpPost]
        [Route("addnotification")]
        public IActionResult AddNotification(Notification notification)
        {
            return Ok(_notificationService.AddNotification(notification));
        }

        [HttpPost]
        [Route("marknotificationread")]
        public IActionResult MarkNotificationAsRead(int notificationId)
        {
            return Ok(_notificationService.MarkNotificationAsRead(notificationId));
        }

    }
}