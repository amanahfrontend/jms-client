﻿using JMS.API.Configurations;
using JMS.API.Constants;
using JMS.BLL.Common;
using JMS.BLL.Helper;
using JMS.BLL.ViewModels;
using JMS.DAL.Common.Enums;
using JMS.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JMS.API.Controllers
{
    //  [Authorize(Roles = "Dispatcher,JMC,JMS Admin")]
    public partial class DriverController : ControllerBase
    {
        private List<DriverModel> driverModels;
        private List<Driver> drivers;
        private DriverModel driverModel;
        private Driver driver;


        [HttpGet]
        [Route("GetDrivers")]
        public IActionResult GetDrivers(string driverName = "") => Ok(_driverService.GetDrivers(driverName));
       
        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll(string driverName = "")
        {
            driverModels = new List<DriverModel>();
            drivers = _driverService.GetBy(e => e.User.FullName.Contains(driverName));

            drivers?.ForEach(driver =>
            {
                driverModel = MappingProfile.Map(driver);
                var journeys = GetDriverActiveJourneys(driver.UserId);
                driverModel.IsHavingActiveJourney = journeys != null && journeys.Count > 0;
                driverModel.ActiveJourneys = journeys;
                driverModels.Add(driverModel);
            });
            return Ok(driverModels);
        }


        [HttpGet("{rowId}")]
        public IActionResult GetById(Guid rowId) => Ok(_driverService.GetFirstOrDefaultBy(e => e.User.RowId == rowId));

        [HttpPost]
        public IActionResult Add(DriverModel driverModel)
        {
            try
            {
                var user = _mapper.Map<User>(driverModel);
                var driver = _mapper.Map<Driver>(driverModel);
                User insertedUser = _userService.Add(user, driverModel.Password);
                driver.UserId = insertedUser.Id;
                Driver insertedDriver = _driverService.Create(driver);
                return Ok(new ServiceResponse { Status = ResponseStatus.Success });
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = ResponseStatus.ServerError, Message = ex.Message });
            }
        }

        [HttpPut]
        public IActionResult Update(DriverModel driverModel)
        {
            try
            {
                var user = _mapper.Map<User>(driverModel);
                if (!user.IsActive && ValidateDriverActiveJourneys(driver.UserId)) return BadRequest(new { message = "Driver has an active Journey" });
                driver = _mapper.Map<Driver>(driverModel);
                _userService.Update(user, driverModel.Password);
                Driver updatedDriver = _driverService.Update(driver);
                return Ok(new ServiceResponse { Status = ResponseStatus.Success });
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = ResponseStatus.ServerError, Message = ex.Message });
            }
        }

        [HttpDelete]
        public IActionResult Delete(Guid rowId)
        {
            try
            {
                driver = _driverService.GetFirstOrDefaultBy(e => e.User.RowId == rowId);
                if (ValidateDriverActiveJourneys(driver.UserId)) return BadRequest(new { message = "Driver has an active Journey" });
                _userService.Delete(rowId);
                _driverService.Delete(driver);
                return Ok(new ServiceResponse { Status = ResponseStatus.Success });
            }
            catch (AppException ex)
            {
                ex.LogException();
                return BadRequest(new ServiceResponse { Status = ResponseStatus.ServerError, Message = ex.Message });
            }
        }

        [HttpGet]
        [Route("ActivateDeactivate")]
        public IActionResult ActivateDeactivate(Guid rowId, bool isActive)
        {
            try
            {
                if (!isActive)
                {
                    driver = _driverService.GetFirstOrDefaultBy(e => e.User.RowId == rowId);
                    if (ValidateDriverActiveJourneys(driver.UserId)) return BadRequest(new { message = "Driver has an active Journey" });
                }
                _userService.ActivateDisactvate(rowId, isActive);
                return Ok(new ServiceResponse { Status = ResponseStatus.Success });
            }
            catch (Exception ex)
            {
                ex.LogException();
                return Ok(new ServiceResponse { Status = JMS.DAL.Common.Enums.ResponseStatus.ServerError });
            }
        }


        private List<KeyValuePair<int, string>> GetDriverActiveJourneys(int driverId)
        {
            var journeys = _journeyService.GetBy(e => e.CurrentDriverId == driverId && _endedJourneyStatuses.Any(s => s != e.JourneyStatus));
            return journeys != null && journeys.Count > 0 ? journeys.Select(e => new KeyValuePair<int, string>(e.Id, e.Title)).ToList() : null;
        }
        private bool ValidateDriverActiveJourneys(int driverId) => _journeyService.ValidateBy(e => e.CurrentDriverId == driverId && _endedJourneyStatuses.Any(s => s != e.JourneyStatus));
    }
}