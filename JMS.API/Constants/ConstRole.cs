﻿namespace JMS.API.Constants
{
    public static class ConstUserRoleType
    {
        public const string ProductLine = "Product Line";
        public const string Dispatcher = "Dispatcher";
        public const string Driver = "Driver";
        public const string JMC = "JMC";
        public const string OperationManager = "Operation Manager";
        public const string QHSE = "QHSE";
        public const string GBM = "GBM";
        public const string JMSAdmin = "JMS Admin";
    }
}
