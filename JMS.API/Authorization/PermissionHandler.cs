﻿using JMS.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace JMS.API.Authorization
{
    internal class PermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        readonly IUserService _userManager;
        readonly IRoleService _roleManager;

        public PermissionAuthorizationHandler(IUserService userManager, IRoleService roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if (context.User == null) return;

            var user = _userManager.GetByName(context.User.Identity.Name);
            //var userRoleNames =  _userManager.GetRoles(user);
            //var userRoles = _roleManager.GetAllRoles().Where(x => userRoleNames.Contains(x.Name));

            //foreach (var role in userRoles)
            //{
            //    var roleClaims = await _roleManager.GetClaimsAsync(role);
            //    var permissions = roleClaims.Where(x => x.Type == CustomClaimTypes.Permission &&
            //                                            x.Value == requirement.Permission &&
            //                                            x.Issuer == "LOCAL AUTHORITY")
            //                                .Select(x => x.Value);

            //    if (permissions.Any())
            //    {
            //        context.Succeed(requirement);
            //        return;
            //    }
            //}



        }
    }
}
