﻿using AutoMapper;
using JMS.BLL.ViewModels;
using JMS.DAL.Entities;

namespace JMS.API.Configurations
{
    public class MappingProfile : Profile
    {
        private readonly static MapperConfiguration config = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<User, DriverModel>();
            cfg.CreateMap<User, ManagerModel>();
        });
        private static IMapper _mapper;

        public MappingProfile()
        {
            _mapper = config.CreateMapper();
            CreateMap<User, UserModel>().ReverseMap();
            CreateMap<RoleModel, Role>().ReverseMap();
            CreateMap<ManagerModel, Manager>().ReverseMap();
            CreateMap<RegisterModel, User>();
            CreateMap<DriverModel, User>().ReverseMap();
            CreateMap<DriverModel, Driver>();
            CreateMap<Driver, DriverModel>();

            //CreateMap<RolePrivilgeViewModel, RolePrivilge>(MemberList.None)
            // .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
            // .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role));
            //CreateMap<RolePrivilge, RolePrivilgeViewModel>(MemberList.None)
            //  .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
            //  .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role));
        }

        public static DriverModel Map(Driver driver)
        {
            DriverModel driverModel = _mapper.Map<DriverModel>(driver.User);
            driverModel.UserId = driver.UserId;
            driverModel.LicenseNo = driver.LicenseNo;
            driverModel.LicenseExpiryDate = driver.LicenseExpiryDate;
            driverModel.GatePassStatus = driver.GatePassStatus;
            driverModel.TrainingDetails = driver.TrainingDetails;
            driverModel.DriveFor = driver.DriveFor;
            driverModel.IsHavingChronicDiseases = driver.IsHavingChronicDiseases;
            driverModel.ChronicDiseases = driver.ChronicDiseases;

            return driverModel;
        }

        public static ManagerModel Map(Manager manager)
        {
            ManagerModel managerModel = _mapper.Map<ManagerModel>(manager.User);
            managerModel.UserId = manager.UserId;
            managerModel.Position = manager.Position;

            return managerModel;
        }
    }
}
