﻿using JMS.BLL.Interfaces;
using JMS.BLL.Services;
using JMS.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace JMS.API.Configurations
{
    public class ApiBuilder
    {
        public void Configure(IServiceCollection services, string dbConnectionString)
        {
            DbContextOptionsBuilder<DbContext> optionsBuilder = new DbContextOptionsBuilder<DbContext>().UseSqlServer(dbConnectionString, x => x.MigrationsAssembly("JMS.DAL"));
            services.AddScoped<DatabaseContext>(provider => new DatabaseContext(optionsBuilder.Options));

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IDriverService, DriverService>();
            services.AddScoped<ICheckpointService, CheckpointService>();
            services.AddScoped<IJourneyService, JourneyService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<IRequestService, RequestService>();
            _ = services.AddScoped<ICommonService, CommonService>();
        }
    }
}
