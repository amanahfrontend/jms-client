using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;

namespace JMS.API
{
    public class Program
    {
        [Obsolete]
        public static void Main(string[] args) => BuildWebHost(args).Run();

        public static IWebHost BuildWebHost(string[] args) => WebHost.CreateDefaultBuilder(args)
               .UseContentRoot(Directory.GetCurrentDirectory())
               .UseIISIntegration()
               .UseStartup<Startup>()
               //.ConfigureLogging((hostingContext, builder) =>
               //{
               //    builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
               //    builder.AddConsole();
               //    builder.AddDebug();
               //    builder.AddNLog();

               //})
            .Build();
        
        public static IHostBuilder CreateHostBuilder(string[] args) =>
                                        Host.CreateDefaultBuilder(args)
                                        .ConfigureWebHostDefaults(webBuilder =>
                                        webBuilder.ConfigureAppConfiguration((hostingContext, config) =>
                                        {
                                            var settings = config.Build();
                                        }).UseStartup<Startup>());
    }
}
