import{UserRoles} from '../../Enums/user-roles.enum'
import{Permission} from '../User/Permission'

export class Role {
    Id: any;
    RowId:string;
    Name: string;
    UserRoleType: UserRoles;
    Permissions: Permission[]
}
