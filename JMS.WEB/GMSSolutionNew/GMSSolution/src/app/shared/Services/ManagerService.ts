import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ServiceResponse } from '../Models/ServiceResponseModel';

@Injectable({ providedIn: 'root' })
export class ManagerService{
   
    constructor(private http: HttpClient) { 
       
    }
     GetManagers(managername=""){
       return  this.http.get(`${environment.JMSApiURL}/manager/getmanagers?managername=${managername}`);    
    }
    Add(model){
        return this.http.post<ServiceResponse<any>>(`${environment.JMSApiURL}/manager/Add`, model).toPromise();
    }
    Update(model){
        return this.http.post<ServiceResponse<any>>(`${environment.JMSApiURL}/manager/Update`, model).toPromise();
    }
    Delete(rowId){
        return this.http.post<ServiceResponse<any>>(`${environment.JMSApiURL}/manager/Delete`, rowId).toPromise(); 
    }
    ActivateOrDeactivate(rowId, isActive){
        return  this.http.get(`${environment.JMSApiURL}/manager/ActivateDeactivate?rowId=${rowId}&isActive=${isActive}`);
    }

}
