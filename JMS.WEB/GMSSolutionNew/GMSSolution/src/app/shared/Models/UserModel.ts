    export interface Role {
        id: string;
        name: string;
    }

    export interface User {
        id: string;
        username: string; 
        email: string;
        password: string;
        firstName: string;
        lastName: string;
        fullName: string;
        userGroupId?: any;
        userWorkForceId?: any;
        licenseNo?: any;
        licenseExpiryDate?: any;
        trainingDetails?: any;
        gatePassStatus?: any;
        token?: string;
        isActive: boolean;
        //role: Role;
        roles: Role[];
        phoneNumber: string;
        emergencyContact: string;
        nationality: string;
        address: string;
        comments: string;
        image:string;
        attachments: Array<string>;
    }





