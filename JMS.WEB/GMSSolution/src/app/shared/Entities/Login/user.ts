﻿import { Role } from "./role";
import { UserRoleType } from "./userRoleType";

export class User {
    id: number;
    username: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    role: Role;
    token?: string;
    fullName: string;
    isActive: boolean;
    phoneNumber: string;
    emergencyContact: string;
    nationality: string;
    address: string;
    comments: string;
    image:string;
    attachments: Array<string>;
    userRoleType: UserRoleType
}