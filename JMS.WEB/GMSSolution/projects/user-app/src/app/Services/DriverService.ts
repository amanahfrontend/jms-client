import { Injectable } from '@angular/core';
import { Driver } from '../Models/Driver';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../Constants';
@Injectable({ providedIn: 'root' })
export class DriverService{ headers:HttpHeaders;
    constructor(private http: HttpClient) { 
        this.headers=new HttpHeaders();
        this.headers.set("Content-Type","application/json");
        this.headers.set("Authorization","Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImJkNGQ1NGUyLTk2NzgtNGQwNi05YzVhLTA4ZDc2N2MzMTAyNCIsInJvbGUiOlsiUEwiLCJEaXNwYXRjaGVyIl0sIm5iZiI6MTU3MzgyNTUyOCwiZXhwIjoxNTc0NDMwMzI4LCJpYXQiOjE1NzM4MjU1Mjh9.Utv9CJdcTtI0NmHix1mLthdepHcyYO8nGtkXM1jZrzs"); 
    }
    GetDrivers(){
        return  this.http.get<Driver[]>(`${Constants.DriverUrl}/getdrivers`,{headers:this.headers});  
    }
    async AddDriver(model:any){
        return await this.http.post(`${Constants.DriverUrl}`,model, { headers: this.headers }).toPromise();
    }
    async UpdateDriver(model: any) {
        return await this.http.put(`${Constants.DriverUrl}`,model, { headers: this.headers }).toPromise();
    }
    DeleteDriver(rowId:string){
        return  this.http.get<Driver[]>(`${Constants.DriverUrl}/GetById/${rowId}`,{headers:this.headers});  
    }

}