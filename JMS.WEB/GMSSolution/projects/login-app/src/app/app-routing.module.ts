// import { RegisterComponent } from './register/register.component';
//import { ResetComponent } from './reset/reset.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path:'',component:LoginComponent},
  //{path:'Register',component:RegisterComponent},
 //{path:'ResetPassword',component:ResetComponent},
  {path:'**',component:LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
